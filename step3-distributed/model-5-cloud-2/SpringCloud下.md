### SpringCloud下

#### 分布式链路追踪

- 应用场景：

  1. 动态展示服务之间调用链路
  2. 分析链路中的性能瓶颈对其调优
  3. 快速定位到调用链路中的故障

- 实现方式

  请求的调用过程中，在每个链路节点中记录日志，并将日志进行集中化可视展示。

- 方案

  - Spring Cloud Sleuth+Twitter Zipkin
  - 阿里”鹰眼“
  - 点评：CAT
  - 美团：Mtrace
  - 京东：Hydra
  - 新浪：Watchman
  - Apache Skywalking

- 核心思想：记录日志

  - Trace：服务追踪单元从发起请求开始，到被追踪系统向客户返回相应的结果。

    ​	TraceId：标识每一个Trace的完整过程，由一个或者多个Span组成

  - Span：每一个处理单元的标识，唯一标识为spanId，多个span有序组成一个Trace

    span还包含一个parentId，用来指向父级span

    span可以认为是一个日志数据结构，同时span抽象了一个概念叫事件：

    - CS：client send/start 客户端/消费端发出一个请求，描述的是一个span开始
    - SR：Server receive/start 服务端/生产者接受请求。**SR-CS属于请求发送的网络延迟**
    - SS：Server send/finish 服务端/生产者发送应答。**SS-SR属于服务端消耗的事件**
    - CR：client receive/finish 客户端/消费者接受应答。**CR-SS表示回复需要的时间（相应的网络延迟）**

- Sleuth+Zikpin

  Sleuth追踪数据，发送给zikpin展示

#### 统一认证方案

- 基于Session的认证方式

  分布式环境下，基于session的认证会出现一个问题，每个应用服务都需要在session中存储用户身份信息，负载均衡的请求都需要带上session信息，否则会需要重新认证。可以通过session共享，session黏贴等方案。

  还有问题是，基于cookie，移动端不能有效使用等

- 基于token的认证方式

  服务端不用存储认证数据，易维护可扩展性强。客户端可以把token存在任意地方，并且可以实现web和app统一认证机制。

  缺点是，token的数据量过大，而且每次请求都需要传递，占用带宽。token的签名也会给CPU带来额外的处理负担

##### OAuth2开放授权协议/标准

应用场景：

- 三方授权登录
- 单点登录

颁发token的授权方式：

- 授权码
- 密码式：提供用户名+密码换取token令牌
- 隐藏式
- 客户端凭证


















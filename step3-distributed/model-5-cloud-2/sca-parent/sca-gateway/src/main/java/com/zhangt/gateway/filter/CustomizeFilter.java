package com.zhangt.gateway.filter;


import cn.hutool.crypto.digest.MD5;
import com.alibaba.nacos.client.utils.IPUtil;
import com.zhangt.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 定义全局过滤器，会对所有路由生效
 */
@Slf4j
@Component  // 让容器扫描到，等同于注册了
public class CustomizeFilter implements GlobalFilter, Ordered {


    // 记录发送的邮件的时间
    private static Map<String, Date> emailMap = new ConcurrentHashMap<>();

    @Reference
    private UserService userService;
    /**
     * 过滤器核心方法
     *
     * @param exchange 封装了request和response对象的上下文
     * @param chain    网关过滤器链（包含全局过滤器和单路由过滤器）
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 从上下文中取出request和response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        // 从request对象中获取客户端ip
        String path = request.getURI().getPath();
        //邮箱发送每个邮箱60s内只允许发送一次
        if (path.startsWith("/api/code/create")) {
            Date now = Calendar.getInstance().getTime();
            String[] splitArr = path.split("/");
            String email = splitArr[splitArr.length - 1];
            if (emailMap.containsKey(email)) {
                Date sendTime = emailMap.get(email);
                Date sendExpireTime = DateUtils.addMinutes(sendTime, 1);
                if (now.before(sendExpireTime)) {
                    response.setStatusCode(HttpStatus.FORBIDDEN); // 状态码
                    String data = "Request will in effect after 60s!";
                    DataBuffer wrap = response.bufferFactory().wrap(data.getBytes());
                    return response.writeWith(Mono.just(wrap));
                }
            } else {
                emailMap.put(email, now);
            }
        }
        else if (path.startsWith("/api/user/login")) {
            String[] splitArr = path.split("/");
            String email = splitArr[splitArr.length - 2];
            String pass = splitArr[splitArr.length - 1];
            String result = userService.login(email, pass);
            if (result.indexOf("@") > 0) {
                return returnOK(response, result );
            } else {
                response.setStatusCode(HttpStatus.FORBIDDEN);
                DataBuffer wrap = response.bufferFactory().wrap(result.getBytes());
                return response.writeWith(Mono.just(wrap));
            }
        } else if (path.startsWith("/api/user/info")) {
            String[] splitArr = path.split("/");
            String token = splitArr[splitArr.length - 1];
            String result = userService.info(token);
            if (result.indexOf("@") > 0) {
                return returnOK(response, result);
            } else {
                response.setStatusCode(HttpStatus.FORBIDDEN);
                DataBuffer wrap = response.bufferFactory().wrap(result.getBytes());
                return response.writeWith(Mono.just(wrap));
            }
        }
        // 合法请求，放行，执行后续的过滤器
        return chain.filter(exchange);
    }

    private Mono<Void> returnOK(ServerHttpResponse response, String result) {
        MultiValueMap<String, ResponseCookie> cookies = response.getCookies();
        ResponseCookie httpCookie = ResponseCookie.from("token", MD5.create().digestHex16(result)).path("/").build();
        cookies.add("loginToken", httpCookie);
        response.setStatusCode(HttpStatus.OK);
        DataBuffer wrap = response.bufferFactory().wrap(result.getBytes());
        return response.writeWith(Mono.just(wrap));
    }

    /**
     * 返回值表示当前过滤器的顺序(优先级)，数值越小，优先级越高
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}

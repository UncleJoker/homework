package com.zhangt.mail.provider;

import com.zhangt.service.MailService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@Service
public class MailProvider implements MailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public Boolean sendMail(String toAddr, String code) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("499145258@qq.com");
        message.setTo(toAddr);
        message.setSubject("邮件验证码");
        message.setText(code);
        javaMailSender.send(message);
        return true;
    }
}

package com.zhangt.consumer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

//    @Value("${self.message}")
//    private String message;
//
//    @GetMapping("/msg")
//    public String viewConfig() {
//        return "config message = "+message;
//    }
}

package com.zhangt.consumer.controller;

import com.zhangt.service.MailService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class MailController {

    @Reference
    private MailService mailService;

    @GetMapping("/send/{toAddr}/{code}")
    public Boolean sendMail(@PathVariable String toAddr, @PathVariable String code){

        return mailService.sendMail(toAddr, code);
    }
}

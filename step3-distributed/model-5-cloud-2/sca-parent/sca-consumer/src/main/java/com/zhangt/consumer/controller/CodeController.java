package com.zhangt.consumer.controller;

import com.zhangt.service.AuthCodeService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/code")
@RestController
public class CodeController {

    @Reference
    private AuthCodeService authCodeService;

    @PostMapping("/create/{email}")
    public Boolean create(@PathVariable String email) {

        return authCodeService.addAuthCode(email);
    }

    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable String email, @PathVariable String code) {

        return authCodeService.validateAuthCode(email, code);
    }
}

package com.zhangt.provider.service.impl;

import com.zhangt.provider.dao.ResumeDao;
import com.zhangt.service.ResumeService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeDao resumeDao;
    @Override
    public Integer findById(Long userId) {
        return resumeDao.findById(userId).get().getUserIdentity();
    }
}

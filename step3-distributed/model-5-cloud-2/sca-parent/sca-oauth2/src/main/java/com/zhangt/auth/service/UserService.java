package com.zhangt.auth.service;

import com.zhangt.pojo.Token;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
public class UserService implements UserDetailsService {

    @Reference
    private com.zhangt.service.UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Token token = userService.findUserByName(s);
        return new User(token.getEmail(), token.getPassword(), new ArrayList<>());
    }
}

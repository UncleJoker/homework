package com.zhangt.service;

public interface AuthCodeService {

    Boolean addAuthCode(String email);

    Integer validateAuthCode(String email, String code);
}

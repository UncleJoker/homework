package com.zhangt.service;

public interface ResumeService {

    Integer findById(Long userId);
}

package com.zhangt.service;

import com.zhangt.pojo.Token;
import com.zhangt.pojo.Users;

public interface UserService {
    Token findUserByName(String userName);

    boolean register(String email, String password, String code);

    boolean isRegister(String email);

    String login(String email, String password);

    String info(String token);
}

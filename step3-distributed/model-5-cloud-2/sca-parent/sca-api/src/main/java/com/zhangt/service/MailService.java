package com.zhangt.service;

public interface MailService {

    Boolean sendMail( String toAddr, String code);
}

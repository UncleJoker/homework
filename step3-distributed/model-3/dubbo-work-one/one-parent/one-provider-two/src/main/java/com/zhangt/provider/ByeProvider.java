package com.zhangt.provider;

import com.zhangt.service.ByeService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;

@DubboService
public class ByeProvider implements ByeService {

    @Override
    public String goodBye(Object... obj) {
        System.out.println("用户调用的goodBye的IP为：" + RpcContext.getContext().getAttachment("userIp"));
        return "bye " + obj[0].toString();
    }
}

package com.zhangt.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import javax.servlet.http.HttpServletRequest;

@Activate(group = {CommonConstants.PROVIDER, CommonConstants.CONSUMER})
public class TransportIPFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        Object[] arguments = invocation.getArguments();
        for (Object argument : arguments) {
            if (argument.getClass().isArray()) {
                Object[] arr = (Object[]) argument;
                for (int i = 0; i < arr.length; i++) {
                    if (arr[i] instanceof HttpServletRequest) {
                        String ip = getIp((HttpServletRequest) arr[i]);
                        RpcContext.getContext().setAttachment("userIp", ip);
                        arr[i] = null;
                    }
                }
            }
        }
        return invoker.invoke(invocation);
    }

    private final String UNKNOWN = "unknown";

    public String getIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            String host = request.getHeader("host");
            if (host.startsWith("localhost")) {
                ip = "127.0.0.1";
            }
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }
}

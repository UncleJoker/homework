package com.zhangt.provider;

import com.zhangt.service.HelloService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.rpc.RpcContext;

@DubboService
public class HelloProvider implements HelloService {

    @Override
    public String sayHello(Object... obj) {

        System.out.println("用户调用的sayHello的IP为："+RpcContext.getContext().getAttachment("userIp"));
        return "hello "+obj[0].toString() ;
    }
}

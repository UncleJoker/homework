package com.zhangt.work.oneweb.controller;

import com.zhangt.service.ByeService;
import com.zhangt.service.HelloService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class HelloController {

    @DubboReference
    private HelloService helloService;

    @DubboReference
    private ByeService byeService;

    @RequestMapping("hello")
    public String hello(HttpServletRequest request) {

        return helloService.sayHello("你好：zhangt", request);
    }

    @RequestMapping("bye")
    public String bye(HttpServletRequest request) {

        return byeService.goodBye("再见", request);
    }
}

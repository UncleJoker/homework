package com.zhangt.work.oneweb;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@DubboComponentScan
public class OneWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(OneWebApplication.class, args);
    }

}

package com.zhangt;

import com.zhangt.consumer.ConsumerComponent;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ConsumerStart {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);
        context.start();
        ConsumerComponent service = context.getBean(ConsumerComponent.class);

        ThreadPoolExecutor poolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            poolExecutor.execute(() -> {
                while (true) {
                    service.bye("bye");
                    service.sayHello("hello");
                    service.say("say");
                }
            });
        }

    }

    @Configuration
    @EnableDubbo(scanBasePackages = "com.zhangt.service")
    @PropertySource("classpath:/dubbo-consumer.properties")
    @ComponentScan(value = {"com.zhangt.consumer"})
    static class ConsumerConfiguration {

    }
}

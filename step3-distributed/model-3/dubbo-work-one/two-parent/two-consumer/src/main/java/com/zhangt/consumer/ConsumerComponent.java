package com.zhangt.consumer;

import com.zhangt.service.HelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

@Component
public class ConsumerComponent {

    @Reference
    private HelloService helloService;

    public String sayHello(String name) {
        return helloService.sayHello(name);
    }

    public String bye(String name) {
        return helloService.goodBye(name);
    }

    public String say(String name) {
        return helloService.speak(name);
    }
}

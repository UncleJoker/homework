package com.zhangt.provider;

import com.zhangt.service.HelloService;
import org.apache.dubbo.config.annotation.Service;

@Service
public class ByeProvider extends ProviderParent implements HelloService {
    @Override
    public String sayHello(Object... obj) {
        super.sleep();
        return "sayHello " + obj[0];
    }

    @Override
    public String goodBye(Object... obj) {
        super.sleep();
        return "goodBye " + obj[0];
    }

    @Override
    public String speak(String content) {
        super.sleep();
        return "speak " + content;
    }
}

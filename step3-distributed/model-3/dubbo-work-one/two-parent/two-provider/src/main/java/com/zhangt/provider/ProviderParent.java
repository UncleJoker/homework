package com.zhangt.provider;

import java.util.Random;

public class ProviderParent {

    private Random random;

    public void sleep() {
        if (random == null) {
            random = new Random();
        }
        int i = random.nextInt(100);
        try {
            System.out.println("本次调用休眠:" + i + " ms");
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

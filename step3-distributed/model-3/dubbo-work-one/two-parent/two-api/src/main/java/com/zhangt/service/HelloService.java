package com.zhangt.service;

public interface HelloService {

    String sayHello(Object... obj);

    String goodBye(Object... obj);

    String speak(String content);
}

package com.zhangt.filter;

import com.zhangt.pojo.TimeCount;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Activate(group = {CommonConstants.CONSUMER})
public class TPMonitorFilter implements Filter {

    private volatile static Map<String, List<TimeCount>> timeMap = new ConcurrentHashMap<>();

    static {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (Map.Entry<String, List<TimeCount>> stringListEntry : timeMap.entrySet()) {
                    synchronized (this) {
                        List<TimeCount> list = stringListEntry.getValue();
                        if (list != null && list.size() > 0) {
                            System.out.println(stringListEntry.getKey() + " 调用次数:" + list.size());
                            List<TimeCount> collectList = list.stream().filter(t -> System.currentTimeMillis() - t.getTimeMill() < (1000 * 60)).collect(Collectors.toCollection(CopyOnWriteArrayList::new));
                            System.out.println(stringListEntry.getKey() + " 一分钟内次数:" + collectList.size());
                            List<TimeCount> sortList = collectList.stream().sorted(Comparator.comparing(TimeCount::getCostTime)).collect(Collectors.toCollection(CopyOnWriteArrayList::new));
                            int tp90Index = (int) Math.floor(sortList.size() * 0.9);
                            int tp99Index = (int) Math.floor(sortList.size() * 0.99);
                            System.out.println(stringListEntry.getKey() + " TP90：" + sortList.get(tp90Index));
                            System.out.println(stringListEntry.getKey() + " TP99：" + sortList.get(tp99Index));
                            timeMap.put(stringListEntry.getKey(), sortList);
                        }
                    }

                }
                System.out.println("_________________________");
            }
        }, 0, 5000);
    }

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        long start = System.currentTimeMillis();
        Result invoke = invoker.invoke(invocation);
        long end = System.currentTimeMillis();
        String methodName = invocation.getMethodName();
        List<TimeCount> list;
        if (timeMap.containsKey(methodName)) {
            list = timeMap.get(methodName);
        } else {
            list = new CopyOnWriteArrayList<>();
            timeMap.put(methodName, list);
        }
        TimeCount timeCount = new TimeCount(start, end - start);
        list.add(timeCount);
        return invoke;
    }
}

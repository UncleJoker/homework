package com.zhangt.pojo;

public class TimeCount {

    //时间戳
    private Long timeMill;

    //消耗的时间
    private Long costTime;

    public TimeCount(Long timeMill, Long costTime) {
        this.timeMill = timeMill;
        this.costTime = costTime;
    }

    public Long getTimeMill() {
        return timeMill;
    }

    public void setTimeMill(Long timeMill) {
        this.timeMill = timeMill;
    }

    public Long getCostTime() {
        return costTime;
    }

    public void setCostTime(Long costTime) {
        this.costTime = costTime;
    }

    @Override
    public String toString() {
        return "TimeCount{" +
                "timeMill=" + timeMill +
                ", costTime=" + costTime +
                '}';
    }
}

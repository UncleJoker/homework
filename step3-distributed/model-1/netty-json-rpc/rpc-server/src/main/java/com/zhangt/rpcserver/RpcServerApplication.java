package com.zhangt.rpcserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

@SpringBootApplication
public class RpcServerApplication implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private NettyServerListener nettyServerListener;

    public static void main(String[] args) {
        SpringApplication.run(RpcServerApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        nettyServerListener.startServer("localhost", 8000);
    }
}

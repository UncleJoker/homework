package com.zhangt.rpcserver;

import com.zhangt.pojo.RpcRequest;
import com.zhangt.rpcserver.handler.UserServerHandler;
import com.zhangt.serial.JSONSerializer;
import com.zhangt.serial.RpcDecoder;
import com.zhangt.serial.RpcEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

@Component
public class NettyServerListener  {

    @Autowired
    private UserServerHandler userServerHandler;

    EventLoopGroup bossGroup = new NioEventLoopGroup();
    ServerBootstrap serverBootstrap = new ServerBootstrap();

    @PreDestroy
    public void close() {
        System.out.println("关闭服务");
        //优雅退出
        bossGroup.shutdownGracefully();
    }

    public void startServer(String hostName, int port) {

        try {
            serverBootstrap.group(bossGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel nioSocketChannel) {
                            ChannelPipeline pipeline = nioSocketChannel.pipeline();
                            pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                            pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                            pipeline.addLast(userServerHandler);
                        }
                    });
            serverBootstrap.bind(hostName, port).sync();
            System.out.println("netty 服务启动。。。。");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

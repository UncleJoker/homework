package com.zhangt.comsumer;

import com.zhangt.handler.UserClientHandler;
import com.zhangt.pojo.RpcRequest;
import com.zhangt.serial.JSONSerializer;
import com.zhangt.serial.RpcDecoder;
import com.zhangt.serial.RpcEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.lang.reflect.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RpcConsumer {

    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private static UserClientHandler clientHandler;

    public Object createProxy(final Class<?> serviceClass) {

        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class<?>[]{serviceClass},
                (proxy, method, args) -> {
                    if(clientHandler == null){
                        initClient();
                    }
                    RpcRequest rpcRequest = new RpcRequest();
                    rpcRequest.setRequestId("10086");
                    rpcRequest.setMethodName(method.getName());
                    rpcRequest.setClassName(serviceClass.getName());
                    Class<?>[] parameterTypes = new Class<?>[args.length];
                    for (int i = 0; i < args.length; i++) {
                        parameterTypes[i] = args.getClass().getComponentType();
                    }
                    rpcRequest.setParameterTypes(parameterTypes);
                    rpcRequest.setParameters(args);
                    clientHandler.setPara(rpcRequest);
//                    System.out.println(clientHandler.getPara());
                    return executor.submit(clientHandler).get();
                });

    }

    public static void initClient() {
        clientHandler = new UserClientHandler();
        Bootstrap bootstrap = new Bootstrap();
        NioEventLoopGroup group = new NioEventLoopGroup();

        bootstrap.group(group).channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) {
                        ChannelPipeline pipeline = channel.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));

                        pipeline.addLast(clientHandler);
                    }
                });

        bootstrap.connect("localhost", 8000).channel();
    }
}

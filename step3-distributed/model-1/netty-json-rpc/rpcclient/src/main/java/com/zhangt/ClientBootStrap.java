package com.zhangt;

import com.zhangt.comsumer.RpcConsumer;
import com.zhangt.service.UserService;

public class ClientBootStrap {

    public static void main(String[] args) throws InterruptedException {
        RpcConsumer rpcConsumer = new RpcConsumer();
        UserService userService = (UserService) rpcConsumer.createProxy(UserService.class);
        while (true) {
            Thread.sleep(2000);

            System.out.println(userService.sayHello("are you ok?"));
        }
    }
}

### SpringCloud上

#### 核心组件：

- 注册中心：Netflix Eureka
- 客户端负载均衡：Netflix ribbon
- 熔断器：Netflix hystrix
- 网关 ：Spring Cloud Gateway
- 配置中心：Spring Cloud Config
- 服务调用：Netflix Feign
- 消息驱动：Spring Cloud Stream
- 链路追踪：Spring Cloud Sleuth/Zipkin

![image-20200702105325373](image-20200702105325373.png)

#### 注册中心

1. 解耦服务消费者和服务提供者
2. 分布式属性，存在多个服务提供者，由注册中心进行管理
3. 消费者获取服务注册信息：
   - poll模式：主动拉取可用的服务提供者清单
   - push模式：服务消费者订阅服务，当服务提供者变化时，注册中心也会主动推送
4. 健康监控，服务失效及时剔除

**注册中心对比：**

![image-20200702141222282](image-20200702141222282.png)

**Eureka：**

- 包含两个组件：Eureka Server和Eureka Client，Eureka Client是个Java客户端，用于简化和Eureka Server的交互；Eureka Server提供服务发现的能力，各个微服务启动时，会通过Eureka Client向Eureka Server进行注册自己的信息（例如网络信息），Eureka Server会存储该服务的信息；

- Client周期性向Eureka Server发送心跳以续约自己的信息，默认周期30秒

- Server在一定时间内没收到心跳，会注销该服务节点，默认时间是90秒

- Client会缓存Server中的信息，即视所有的Eureka Server节点都宕掉，消费者依然可以使用缓存中的信息找到服务提供者

- 自我保护机制：在15分钟内超过85%的客户端节点都没有正常心跳，那么Eureka就认为客户端与注册中心出现了网络故障，会进入自我保护机制。因为Eureka与客户端网络问题，不意味着服务提供者和消费者之间的网络是有问题的。

  1. 不会剔除任何服务实例，保证大多数服务依然可用
  2. 可以接受性服务注册和查询请求，但是不会同步到其他节点上，保证当前节点依然可用，当网络稳定后再同步
  3. 可以配置eureka.server.enable-self-preservation=false关闭自我保护

- 启动流程：

  1. 依托Spring Boot，通过spring.factories配置的EurekaServerAutoConfiguration自动装配

  2. 标记**EurekaServerMarkerConfiguration.Marker**才能作为Eureka Server启动。

  3. **EurekaController**：对外接口，Eureka后台界面

  4. **PeerAwareInstanceRegistry**：对节点感知实例注册器，集群模式下注册服务使用的。EurekaServer集群各个节点是对等的，没有主从

  5. **PeerEurekaNodes**：辅助封装对等节点相关的信息和操作，比如更新集群当中的对等节点，核心是PeerEurekaNodes.start()方法

  6. **EurekaServerContext**：注入Eureka Server的上下文--DefaultEurekaServerContext，DefaultEurekaServerContext初始化之后会执行initialize()方法，initialize()方法中主要启动了PeerEurekaNodes.start()方法

  7. **EurekaServerBootstrap**：后续启动使用该对象

  8. **FilterRegistrationBean**：注册Jersey过滤器，发布restful服务接口

  9. **EurekaServerInitializerConfiguration**继承SmartLifecycle，可以在Spring容器的Bean创建完成之后执行start()方法

  10. EurekaServerInitializerConfiguration.start()中**初始化EurekaServerContext**的细节：调用EurekaServerBootstrap#contextInitialized方法

  11. EurekaServerBootstrap#contextInitialized调用initEurekaServerContext()方法初始化上下文细节

  12. **initEurekaServerContext()**方法：

      1. EurekaServerContextHolder.initialize(this.serverContext);//为非IOC容器获取ServerContext对象的接口

      2. ```java
         // Copy registry from neighboring eureka node
         //当某一个Server启动时，从其他Server拷贝注册信息过来，每一个Server相对于其他Server来说也是客户端
         int registryCount = this.registry.syncUp();
         //更改实例状态为UP，对外提供服务
         this.registry.openForTraffic(this.applicationInfoManager, registryCount);
         // Register all monitoring statistics.
         //注册统计器
         EurekaMonitors.registerAllStats();
         ```

  13. Server服务接口暴露策略，Eureka启动过程中，注册了Jersey框架，通过扫描指定包目录获得相应的资源

  14. Server服务注册接口，接受服务端注册服务：

      1. ApplicationResource类的addInstance()方法中代码：registry.register(info, "true".equals(isReplication));
      2. com.netflix.eureka.registry.PeerAwareInstanceRegistryImpl#register 注册服务信息并同步到其他Eureka节点。
      3. PeerAwareInstanceRegistryImpl调用父类com.netflix.eureka.registry.AbstractInstanceRegistry#register注册，实例信息存储在ConcurrentHashMap中
      4. com.netflix.eureka.registry.PeerAwareInstanceRegistryImpl#replicateToPeers：同步注册信息到其他Eureka节点

      


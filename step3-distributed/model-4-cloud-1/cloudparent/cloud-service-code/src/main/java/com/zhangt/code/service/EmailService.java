package com.zhangt.code.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "service-mail")
public interface EmailService {

    @RequestMapping(value = "/email/send/{email}/{code}", method = RequestMethod.GET)
    Boolean sendEmail(@PathVariable String email, @PathVariable String code);
}

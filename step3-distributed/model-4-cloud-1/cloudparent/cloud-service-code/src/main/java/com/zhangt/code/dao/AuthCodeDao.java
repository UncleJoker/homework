package com.zhangt.code.dao;

import com.zhangt.pojo.AuthCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthCodeDao extends JpaRepository<AuthCode, Integer> {
}

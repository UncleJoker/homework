package com.zhangt.code.service.impl;

import com.zhangt.code.dao.AuthCodeDao;
import com.zhangt.code.service.AuthCodeService;
import com.zhangt.code.service.EmailService;
import com.zhangt.pojo.AuthCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

@Service
public class AuthCodeServiceImpl implements AuthCodeService {

    @Autowired
    private EmailService emailService;
    @Autowired
    private AuthCodeDao authCodeDao;

    @Override
    public Boolean addAuthCode(String email) {
        String code = new Random().nextInt(999999) + "";
        Boolean s = emailService.sendEmail(email, code);
        if (s) {
            AuthCode authCode = new AuthCode();
            authCode.setEmail(email);
            authCode.setCode(code);
            Calendar instance = Calendar.getInstance();
            Date now = instance.getTime();
            authCode.setCreatetime(now);
            instance.add(Calendar.MINUTE, 5);
            authCode.setExpiretime(instance.getTime());
            authCodeDao.save(authCode);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Integer validateAuthCode(String email, String code) {
        AuthCode authCode = new AuthCode();
        authCode.setEmail(email);
        authCode.setCode(code);
        Example<AuthCode> example = Example.of(authCode);
        Optional<AuthCode> one = authCodeDao.findOne(example);
        if (one.isPresent()) {
            AuthCode existsCode = one.get();
            authCodeDao.deleteById(existsCode.getId());
            if (existsCode.getExpiretime().after(Calendar.getInstance().getTime())) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 0;
        }

    }
}

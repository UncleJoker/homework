package com.zhangt.code.controller;

import com.zhangt.code.service.AuthCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/code")
@RestController
public class CodeController {

    @Autowired
    private AuthCodeService authCodeService;

    @PostMapping("/create/{email}")
    public Boolean create(@PathVariable String email) {

        return authCodeService.addAuthCode(email);
    }

    @GetMapping("/validate/{email}/{code}")
    public Integer validate(@PathVariable String email, @PathVariable String code) {

        return authCodeService.validateAuthCode(email, code);
    }
}

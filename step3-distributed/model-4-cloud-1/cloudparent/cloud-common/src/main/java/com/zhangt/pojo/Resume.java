package com.zhangt.pojo;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "r_resume")
@ToString
public class Resume implements Serializable {
    @Id
    private Long id;
    private String sex;
    private String birthday; // ኞ෭
    private String work_year; // ૡ֢ଙᴴ
    private String phone; // ݩಋ๢
    private String email; // ᮒᓟ
    private String status; // ፓڹᇫா
    private String resumeName; // ᑍݷܲᓌ
    private String name; // ݷন
    private String createTime; // ڠୌ෸ᳵ
    private String headPic; // ؟१
    private Integer isDel; //ᴻڢވฎἕᦊ꧊0-ᴻڢ๚ 1-ᴻڢ૪
    private String updateTime; // ᓌܲๅෛ෸ᳵ
    private Long userId; // አಁID
    private Integer isDefault; // ฎވԅἕᦊᓌܲ 0-ἕᦊ 1-ᶋἕᦊ
    private String highestEducation; // ๋ṛ਍ܲ
    private Integer deliverNearByConfirm; // ಭ᭓ᴫկᓌܲᏟᦊ 0-ᵱᥝᏟᦊ 1-ӧᵱᥝᏟᦊ
    private Integer refuseCount; // ᓌܲᤩ೏ᕷེහ
    private Integer markCanInterviewCount; //ᤩຽᦕԅݢᶎᦶེහ
    private Integer haveNoticeInterCount; //૪᭗Ꭳᶎᦶེහ
    private String oneWord; // ӞݙᦾՕᕨᛔ૩
    private String liveCity; // ੷֘उ૱
    private Integer resumeScore; // ړ஑ܲᓌ
    private Integer userIdentity; // አಁ᫝ղ1-਍ኞ 2-ૡՈ
    private Integer isOpenResume; // Ո಍൤ᔱ-୏නᓌܲ 0-ᳮ҅ى1-಑୏҅2-ಭک๚ᬡܲᓌᳮىᤩۖاٴනຽ 3-՗๚ᦡᗝᬦ୏නᓌܲ
}
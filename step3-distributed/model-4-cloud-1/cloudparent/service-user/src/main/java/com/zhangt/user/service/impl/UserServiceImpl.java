package com.zhangt.user.service.impl;

import cn.hutool.crypto.digest.MD5;
import com.zhangt.pojo.Token;
import com.zhangt.user.dao.TokenDao;
import com.zhangt.user.service.CodeService;
import com.zhangt.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private CodeService codeService;
    @Autowired
    private TokenDao tokenDao;

    @Override
    public boolean register(String email, String password, String code) {
        if (codeService.validate(email, code) == 1) {
            Token token = new Token();
            token.setEmail(email);
            token.setPassword(MD5.create().digestHex(email + password));
            tokenDao.save(token);
            return true;
        }
        return false;
    }

    @Override
    public boolean isRegister(String email) {
        Token token = new Token();
        token.setEmail(email);
        Example<Token> example = Example.of(token);
        return tokenDao.findOne(example).isPresent();
    }

    @Override
    public String login(String email, String password) {
        String passAfter = MD5.create().digestHex(email + password);
        Token token = new Token();
        token.setEmail(email);
        Example<Token> example = Example.of(token);
        Optional<Token> one = tokenDao.findOne(example);
        if (one.isPresent()) {
            token = one.get();
            if (token.getPassword().equals(passAfter)) {
                String loginToken  = MD5.create().digestHex16(email);
                token.setToken(loginToken);
                tokenDao.save(token);
                return email;
            } else {
                return "wrong pass";
            }
        }
        return "no such user";
    }

    @Override
    public String info(String token) {
        Token user = new Token();
        user.setToken(token);
        Example<Token> example = Example.of(user);
        Optional<Token> one = tokenDao.findOne(example);
        return one.map(Token::getEmail).orElse("no such user");
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            String s = MD5.create().digestHex("zhangt@shiningchip.cn123456");
            System.out.println(s);
        }
    }
}

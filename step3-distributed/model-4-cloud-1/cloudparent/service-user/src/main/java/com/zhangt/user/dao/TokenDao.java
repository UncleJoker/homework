package com.zhangt.user.dao;

import com.zhangt.pojo.Token;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenDao extends JpaRepository<Token, Integer> {
}

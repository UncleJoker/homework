package com.zhangt.user.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "service-code")
public interface CodeService {

    @RequestMapping(value = "/code/create/{email}", method = RequestMethod.POST)
    String createCode(@PathVariable String email);

    @RequestMapping(value = "/code/validate/{email}/{code}", method = RequestMethod.GET)
    Integer validate(@PathVariable String email, @PathVariable String code);
}

package com.zhangt.user.controller;

import com.zhangt.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register/{email}/{password}/{code}")
    public Boolean register(@PathVariable String email, @PathVariable String password, @PathVariable String code) {
        return userService.register(email, password, code);
    }

    @GetMapping("/isRegister/{email}")
    public Boolean isRegister(@PathVariable String email) {
        return userService.isRegister(email);
    }

    @PostMapping("/login/{email}/{password}")
    public String login(@PathVariable String email, @PathVariable String password) {
        return userService.login(email, password);
    }

    @GetMapping("/info/{token}")
    public String info(@PathVariable String token) {
        return userService.info(token);
    }
}

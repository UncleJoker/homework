package com.zhangt.mail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/email")
@RestController
public class SendMailController {

    @Autowired
    private JavaMailSender javaMailSender;

    @GetMapping("/send/{toAddr}/{code}")
    public Boolean sendMail(@PathVariable String toAddr, @PathVariable String code){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("499145258@qq.com");
        message.setTo(toAddr);
        message.setSubject("邮件验证码");
        message.setText(code);
        javaMailSender.send(message);
        return true;
    }
}

package com.zhangt.cloudgetway.filter;

import com.zhangt.util.IPUtil;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class HostAddrKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
//        String ip = IPUtil.getIp(exchange.getRequest());
//        System.out.println(ip);
//        return Mono.just(ip);
        return Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
    }
}

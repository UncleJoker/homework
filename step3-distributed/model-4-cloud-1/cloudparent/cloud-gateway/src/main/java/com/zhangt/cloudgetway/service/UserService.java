package com.zhangt.cloudgetway.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "service-user")
public interface UserService {

    @RequestMapping(value = "/user/info/{token}", method = RequestMethod.GET)
    String info(@PathVariable String token);

    @RequestMapping(value = "/user/login/{email}/{password}", method = RequestMethod.POST)
    String login(@PathVariable String email, @PathVariable String password);

}

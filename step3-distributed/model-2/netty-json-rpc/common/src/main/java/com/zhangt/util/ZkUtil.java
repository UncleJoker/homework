package com.zhangt.util;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;

@Component
public class ZkUtil {

    public static final String servicePath = "/rpc/service";

    private ZkClient zkClient;

    public ZkUtil() {
        this.zkClient = new ZkClient("139.199.19.5:2181");
    }

    public void writeService(String childPath, String host) {
        String path = servicePath + "/" + childPath;
        if (!zkClient.exists(path)) {
            zkClient.createPersistent(path, true);
        }
        String ipPath = path + "/" + host;
        if (!zkClient.exists(ipPath)) {
            zkClient.createEphemeral(ipPath, Calendar.getInstance().getTimeInMillis()/1000);
        }
    }

    public Object readData(String path) {
        boolean exists = zkClient.exists(path);
        if (!exists) {
            return "";
        }
        return zkClient.readData(path);
    }

    public List<String> readChild(String path) {
        boolean exists = zkClient.exists(path);
        if (!exists) {
            return null;
        }
        return zkClient.getChildren(path);
    }

    public ZkClient getZkClient() {
        return zkClient;
    }

    public static String firstLowCase(String s) {

        if (Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }

    public static String firstUpCase(String s) {

        if (Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }
}

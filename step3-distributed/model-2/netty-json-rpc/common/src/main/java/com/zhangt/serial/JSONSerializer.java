package com.zhangt.serial;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.zhangt.pojo.RpcRequest;
import com.zhangt.service.Serializer;

public class JSONSerializer implements Serializer {

    @Override
    public byte[] serialize(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        return JSONObject.parseObject(bytes, clazz);
    }

    public static void main(String[] args) {
        RpcRequest rpcRequest = new RpcRequest();
        rpcRequest.setClassName("com.zhangt.ClientBootStrap");
        rpcRequest.setMethodName("main");
        rpcRequest.setRequestId("1");
        JSONSerializer jsonSerializer = new JSONSerializer();
        byte[] serialize = jsonSerializer.serialize(rpcRequest);
        for (int i = 0; i < serialize.length; i++) {
            System.out.print(serialize[i]);
        }
        System.out.println();
    }
}

package com.zhangt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZkDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZkDbApplication.class, args);
    }

}

package com.zhangt.runner;

import com.alibaba.druid.pool.DruidDataSource;
import com.zhangt.config.DatasourceConfig;
import com.zhangt.util.ZkUtil;
import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;

@Component
public class DbRunner implements CommandLineRunner {
    @Autowired
    private ZkUtil zkUtil;
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        ZkClient zkClient = zkUtil.getZkClient();
        String parentPath = DatasourceConfig.dbConfigPath;
        List<String> children = zkClient.getChildren(parentPath);
        children.forEach(c -> {
            zkClient.subscribeDataChanges(parentPath + "/" + c, new IZkDataListener() {
                @Override
                public void handleDataChange(String path, Object o) throws Exception {
                    String value = (String) o;
                    System.out.println("连接池变更，内容：" + path + ":" + value);
                    datasourceChange(c, value);
                }

                @Override
                public void handleDataDeleted(String s) throws Exception {
                    System.out.println(s + "被删除");
                }
            });
        });
    }

    private void datasourceChange(String name, String value) throws Exception {
        DruidDataSource datasource = (DruidDataSource) applicationContext.getBean("dataSource");
        String methodName = "set" + ZkUtil.firstUpCase(name);
        Method method = datasource.getClass().getMethod(methodName, String.class);
        method.invoke(datasource, value);
    }
}

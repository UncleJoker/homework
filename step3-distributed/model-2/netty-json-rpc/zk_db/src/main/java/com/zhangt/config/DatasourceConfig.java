package com.zhangt.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.zhangt.util.ZkUtil;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.lang.reflect.Method;
import java.util.List;

@Configuration
public class DatasourceConfig {

    @Autowired
    private ZkUtil zkUtil;

    public static final String dbConfigPath = "/config/db";

    private String url;

    private String username;

    private String password;

    private String driverClassName;


    public DatasourceConfig() {
        this.driverClassName = "com.mysql.cj.jdbc.driver";
    }

    @Bean     //声明其为Bean实例
    @Primary  //在同样的DataSource中，首先使用被标注的DataSource
    public DataSource dataSource() throws Exception {
        getDbConfig();
        DruidDataSource datasource = new DruidDataSource();
        datasource.setUrl(url);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setDriverClassName(driverClassName);
        System.out.println("连接池启动");
        return datasource;
    }

    public void getDbConfig() throws Exception {
        List<String> strings = zkUtil.readChild(dbConfigPath);
        for (String childPath : strings) {
            String path= dbConfigPath+"/"+childPath;
            String string = (String) zkUtil.readData(path);
            String methodName = "set"+ZkUtil.firstUpCase(childPath);
            Method method = this.getClass().getMethod(methodName, String.class);
            method.setAccessible(true);
            method.invoke(this, string);
        }
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public static void main(String[] args) {
        ZkClient zkClient = new ZkClient("139.199.19.5:2181");
        zkClient.writeData("/config/db/url","jdbc:mysql://localhost:3306/zhangt23");
        zkClient.writeData("/config/db/username","root3");
        zkClient.writeData("/config/db/password","root@12354");
    }

}

package com.zhangt.comsumer;

import com.zhangt.handler.UserClientHandler;
import com.zhangt.pojo.RpcRequest;
import com.zhangt.serial.JSONSerializer;
import com.zhangt.serial.RpcDecoder;
import com.zhangt.serial.RpcEncoder;
import com.zhangt.util.ZkUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class RpcConsumer {

    @Autowired
    private ZkUtil zkUtil;

    private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private Map<String, UserClientHandler> clientHandlerMap;

    private Map<String, Channel> channelMap;

    public RpcConsumer() {
        clientHandlerMap = new HashMap<>();
        channelMap = new HashMap<>();
    }

    public Object createProxy(final Class<?> serviceClass) {

        return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class<?>[]{serviceClass},
                (proxy, method, args) -> {
                    UserClientHandler clientHandler = getFromList(serviceClass.getSimpleName());

                    RpcRequest rpcRequest = new RpcRequest();
                    rpcRequest.setRequestId("10086");
                    rpcRequest.setMethodName(method.getName());
                    rpcRequest.setClassName(serviceClass.getName());
                    Class<?>[] parameterTypes = new Class<?>[args.length];
                    for (int i = 0; i < args.length; i++) {
                        parameterTypes[i] = args.getClass().getComponentType();
                    }
                    rpcRequest.setParameterTypes(parameterTypes);
                    rpcRequest.setParameters(args);
                    clientHandler.setPara(rpcRequest);
                    return executor.submit(clientHandler).get();
                });

    }

    public void initClient(String hostName, String port) {

        UserClientHandler clientHandler = new UserClientHandler();
        Bootstrap bootstrap = new Bootstrap();
        NioEventLoopGroup group = new NioEventLoopGroup();

        bootstrap.group(group).channel(NioSocketChannel.class)
                .option(ChannelOption.TCP_NODELAY, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) {
                        ChannelPipeline pipeline = channel.pipeline();
                        pipeline.addLast(new RpcEncoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(new RpcDecoder(RpcRequest.class, new JSONSerializer()));
                        pipeline.addLast(clientHandler);
                    }
                });
        String key = hostName + ":" + port;
        clientHandlerMap.put(key, clientHandler);
        Channel channel = bootstrap.connect(hostName, Integer.parseInt(port)).channel();
        channelMap.put(key, channel);
    }

    public UserClientHandler getFromList(String serviceName) {
        UserClientHandler userClientHandler = null;
        String key = null;
        //记录时间
        Long mills = Calendar.getInstance().getTimeInMillis() / 1000;
        Iterator<Map.Entry<String, UserClientHandler>> it = clientHandlerMap.entrySet().iterator();
        List<UserClientHandler> list = new ArrayList<>();
        //遍历map，找出满足响应时间为0，或者上次响应时间跟此次差距大于等于5秒的对象
        while (it.hasNext()) {
            Map.Entry<String, UserClientHandler> next = it.next();
            UserClientHandler clientHandler = next.getValue();
            if (clientHandler.getMills() == 0 || (mills - clientHandler.getMills() >= 5)) {
                userClientHandler = clientHandler;
                key = next.getKey();
                break;
            } else {
                clientHandler.setHost(next.getKey());
                list.add(clientHandler);
            }
        }
        //未找到，遍历list，根据时间排序
        if (userClientHandler == null) {
            list.sort((o1, o2) -> o2.getMills().compareTo(o1.getMills()));
            userClientHandler = list.get(0);
            key = userClientHandler.getHost();
        }
        System.out.println("本次调用选举的服务："+key);
        String path = ZkUtil.servicePath + "/" + ZkUtil.firstLowCase(serviceName) + "/" + key;
        zkUtil.getZkClient().writeData(path, mills);
        userClientHandler.setMills(mills);
        return userClientHandler;
    }

    public Map<String, Channel> getChannelMap() {
        return channelMap;
    }

    public Map<String, UserClientHandler> getClientHandlerMap() {
        return clientHandlerMap;
    }
}

package com.zhangt;

import com.zhangt.comsumer.RpcConsumer;
import com.zhangt.handler.UserClientHandler;
import com.zhangt.service.UserService;
import com.zhangt.util.ZkUtil;
import io.netty.channel.Channel;
import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

@SpringBootApplication
public class ClientBootStrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ZkUtil zkUtil;
    @Autowired
    private RpcConsumer rpcConsumer;

    public static void main(String[] args) {

        SpringApplication.run(ClientBootStrap.class, args);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        ZkClient zkClient = zkUtil.getZkClient();
        List<String> strings = zkUtil.readChild(ZkUtil.servicePath);
        if (!CollectionUtils.isEmpty(strings)) {
            strings.forEach(s -> {
                String childPath = ZkUtil.servicePath + "/" + s;
                List<String> paths = zkUtil.readChild(childPath);
                paths.forEach(p -> {
                    String[] strArr = p.split(":");
                    rpcConsumer.initClient(strArr[0], strArr[1]);
                });
                zkClient.subscribeChildChanges(childPath, (s1, list) -> {
                    System.out.println("子节点变更：" + s1);
                    Map<String, Channel> channelMap = rpcConsumer.getChannelMap();
                    Map<String, UserClientHandler> clientHandlerMap = rpcConsumer.getClientHandlerMap();

                    clientHandlerMap.keySet().removeIf(key -> !list.contains(key));
                    channelMap.keySet().removeIf(key -> {
                        if (!list.contains(key)) {
                            Channel channel = channelMap.get(key);
                            channel.closeFuture();
                            return true;
                        } else {
                            return false;
                        }
                    });
                    for (String childPath1 : list) {
                        if (!clientHandlerMap.containsKey(childPath1)) {
                            Long mills = (Long) zkUtil.readData(childPath+ "/" + childPath1);
                            System.out.println(mills);
                            String[] strArr = childPath1.split(":");
                            rpcConsumer.initClient(strArr[0], strArr[1]);
                        }
                    }
                });
            });
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        UserService userService = (UserService) rpcConsumer.createProxy(UserService.class);
        while (true) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String s = userService.sayHello("are you ok?");
            System.out.println(s);
        }
    }
}

package com.zhangt.handler;

import com.zhangt.pojo.RpcRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.Callable;

public class UserClientHandler extends ChannelInboundHandlerAdapter implements Callable {
    private ChannelHandlerContext context;
    private String result;
    private RpcRequest para;
    private Long mills;
    private String host;
    public UserClientHandler() {
        this.mills = 0L;
    }

    @Override
    public  void channelActive(ChannelHandlerContext ctx) {
        System.out.println("初始化######");
        context = ctx;
    }

    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) {
        result = msg.toString();
        notify();
    }
    @Override
    public synchronized Object call() {
        System.out.println("读取#####");
        try {
            context.writeAndFlush(para);
            wait();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public void setPara(RpcRequest para) {
        this.para = para;
    }

    public RpcRequest getPara() {
        return para;
    }

    public Long getMills() {
        return mills;
    }

    public void setMills(Long mills) {
        this.mills = mills;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}

package com.zhangt.rpcserver;

import com.zhangt.util.ZkUtil;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Set;

@Component
public class ZkRegister {

    @Autowired
    private ZkUtil zkUtil;

    public void register(String hostName, String port) {
        String packageName = "com.zhangt";

            Reflections reflections = new Reflections(packageName);
            Set<Class<?>> autoSets = reflections.getTypesAnnotatedWith(Service.class);
            for (Class<?> autoSet : autoSets) {
                String key = autoSet.getSimpleName();
                if (autoSet.getInterfaces().length > 0) {
                    key = autoSet.getInterfaces()[0].getSimpleName();
                }
                if (!autoSet.getAnnotation(Service.class).value().equals("")) {
                    key = autoSet.getAnnotation(Service.class).value();
                }
                key = ZkUtil.firstLowCase(key);
                String host = hostName + ":" + port;
                zkUtil.writeService(key, host);
        }
    }


}

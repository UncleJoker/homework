package com.zhangt.rpcserver.impl;

import com.zhangt.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public String sayHello(String param) {
        String str = "调用成功--参数："+param;
        System.out.println(str);
        return "success";
    }

}

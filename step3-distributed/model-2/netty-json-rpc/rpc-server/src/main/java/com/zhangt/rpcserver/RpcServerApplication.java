package com.zhangt.rpcserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextRefreshedEvent;

@SpringBootApplication
@ComponentScan({"com.zhangt"})
public class RpcServerApplication implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private NettyServerListener nettyServerListener;
    private static String port;

    public static void main(String[] args) {
        port = args[0];
        SpringApplication.run(RpcServerApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        nettyServerListener.startServer("localhost", Integer.parseInt(port));
    }
}

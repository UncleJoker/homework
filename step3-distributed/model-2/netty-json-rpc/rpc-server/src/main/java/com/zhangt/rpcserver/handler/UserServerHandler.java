package com.zhangt.rpcserver.handler;

import com.zhangt.pojo.RpcRequest;
import com.zhangt.serial.JSONSerializer;
import com.zhangt.service.UserService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@ChannelHandler.Sharable
public class UserServerHandler extends ChannelInboundHandlerAdapter {

    @Autowired
    private UserService userService;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        JSONSerializer jsonSerializer = new JSONSerializer();
        RpcRequest rpcRequest = jsonSerializer.deserialize(RpcRequest.class, jsonSerializer.serialize(msg));
        String result = userService.sayHello((String) rpcRequest.getParameters()[0]);
        ctx.writeAndFlush(result);

    }
}

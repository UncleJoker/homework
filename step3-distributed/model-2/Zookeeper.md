### Zookeeper

#### 基本概念

 1. 集群角色：

    Leader，Follower，Observer三种角色。Leader提供读写，其他角色提供读服务，Observer不参与leader选举过程以及写操作的过半成功策略。

 2. 会话（session）

    客户端会话。**一个客户端连接是指客户端和服务端之间的一个TCP长连接**。客户端的第一次连接开始，开启生命周期，客户端通过心跳检测与服务端保持有效的会话，能够向zk发送请求并响应，同时还可以通过该连接接收来自服务器的Watch事件通知。

 3. 数据节点（ZNode）

    zk将数据存在内存中，存储模型是树形结构（ZNode tree），斜杠（/）分割

 4. 版本

    zk的每个ZNode上都会存储数据，同时会维护一个Stat的数据结构，记录了ZNode的三个数据版本，分别是version（znode当前版本）、cversion（子节点版本）、aversion（当前ZNode的acl版本）

 5. Watcher机制

    zk允许用户在指定节点上注册一些watcher，触发特定事件时，zk会将事件通知到感兴趣的客户端

    ![image-20200530203926404](image-20200530203926404.png)

 6. ACL（Access control list）

    + CREATE：创建子节点的权限
    + Read：读取节点数据和子节点列表
    + Write：更新节点数据
    + Delete：删除子节点
    + Admin：设置节点ACL的权限

    使用**“scheme: id : permission”**来标识一个有效的ACL信息，有四种模式：

    + IP：通过IP地址来进行权限控制
    + Digest：最常用的权限控制模式，**username:password** 来表示权限
    + World：最开放的权限控制模式，可以看成特殊的Digest模式，只有一个权限表示**“world:anyone”**.
    + Super：超级用户，特殊的Digest，可以进行任何操作

#### ZNode类型

分为三大类：

1. 持久性节点
2. 临时性节点
3. 顺序性节点

可以通过组合生成四种节点类型：持久节点、持久顺序节点、临时节点、临时顺序节点

#### 事务ID

zk中，事务是指能够改变zk服务器状态的操作，成为事务操作或者更新操作，包括节点创建、删除、节点数据更新等操作。每个事务请求，zk会为其分配一个全局的事务ID，用ZXID来表示，通常是一个64位的数字。每一个ZXID对应一次事务操作，zk可以通过zxid试别处理顺序。

#### ZNode的状态信息

ZNode节点内容包含两部分：节点数据内容和节点状态信息。[quota]是状态信息。

- cZxid：节点被创建时的事务ID
- ctime：节点创建时间
- mZxid：最后一次被修改的事务ID
- mtime：最后一次被修改的时间
- pZxid：子节点最后一次被修改的事务ID，子节点列表变更才会更新，内容变更不会更新
- CVersion：子节点版本
- dataVersion：数据内容版本
- aclVersion：ACL版本
- ephemeralOwner：临时节点的会话sessionID，持久节点的值为0
- dataLength：数据长度
- numChildren：表示直系子节点数

#### ZAB协议

核心是定义了对于那些会改变ZK服务器数据状态的事务请求的处理方式。即：所有事务请求必须由全局唯一的服务器（Leader）来协调处理，将客户端的事务请求转化成一个事务的Proposal，发并发送给follower服务器，半数follower服务器进行了正确的反馈后，leader发出commit消息，要求提交该事务。![image-20200531233214002](image-20200531233214002.png)

ZAB协议包含两种基本的模式：**崩溃恢复和消息广播**

- 崩溃恢复：ZK启动，或者leader服务器出现中断、崩溃或者重启等异常情况的时候，ZAB会进入崩溃恢复模式，选举产生新的leader，当过半机器与leader进行状态同步之后，退出恢复模式。状态同步指的是数据同步，用来保证过半机器能够和leader服务器的数据状态保持一致。
- 消息广播：使用原子广播协议，类似于一个二阶段提交过程 ，leader服务器会将客户端的事务请求，生成对应的proposal，并将其发送给其余的所有机器，收集选票后，过半同意最后进行事务提交。**过程：**每个事务请求会生成一个全局唯一ID：ZXID。leader服务器会为每一个follower服务器都分配一个单独的队列，然后将需要广播的事务Proposal一次放入队列中，并根据FIFO策略发送。**超过半数follower成功将事务写入到本地磁盘中并响应ACK，那么leader会发送commit请求，让follower服务器进行事务提交。**不需要等待所有的服务器响应，所以ZAB协议移除了中断逻辑。但是无法解决leader服务器崩溃退出导致数据不一致的问题，**所以ZAB在leader退出之后会进行崩溃恢复的过程**。

特性：

1. **ZAB协议需要确保那些以及在leader服务器上提交的事务最终被所有服务器提交**
2. **确保丢弃那些只在leader服务器上被提出的事务。**

**ZAB协议在完成leader选举之后，正式工作之前，会先完成数据同步：**

​	**leader为所有的follower准备一个队列，并将那些没有被各follower服务器同步的事务再次发送，并且紧接着发一个commit，表示该事务已经被提交。follower服务器同步了所有的事务proposal并返回ACK之后，leader服务器才会将该follower加入到真正的可用follower列表中。**


package com.zhangt.generate;

import com.zhangt.util.RedisUtil;
import com.zhangt.util.SpringUtil;
import org.apache.shardingsphere.spi.keygen.ShardingKeyGenerator;

import java.util.Properties;


public class IdGenerator implements ShardingKeyGenerator {

    private Properties properties;
    @Override
    public Comparable<?> generateKey() {
        System.out.println("执行自定义主键生成器");
        RedisUtil redisUtil = SpringUtil.getBean(RedisUtil.class);
        return redisUtil.incr(properties.getProperty("table"), 1);
    }

    @Override
    public String getType() {
        return "REDISID";
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}

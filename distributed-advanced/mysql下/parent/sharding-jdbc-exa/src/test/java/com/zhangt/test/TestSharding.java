package com.zhangt.test;

import com.zhangt.RunBoot;
import com.zhangt.entity.Order;
import com.zhangt.repository.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestSharding {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testAdd(){
        Random random = new Random();

        for (int i = 0; i < 200; i++) {
            Order order = Order.builder().isDel(1).companyId(2).positionId(3)
                    .publishUserId(4).resumeType(5).userId(random.nextInt(99999)).createTime(new Date()).updateTime(new Date()).status("1").build();
            orderRepository.save(order);
        }
    }

    @Test
    public void query(){
        List<Order> all = orderRepository.findAll();
        System.out.println("结果集长度："+all.size());
    }
}

### MySql集群搭建：一主二从一MHA

1. 配置虚拟机：

   虚拟机网络配置：

   ```
   HWADDR=00:0c:29:f6:3b:77
   IPADDR=192.168.221.128
   NETMASK=255.255.255.0
   GATEWAY=192.168.221.2
   ```

   ```
   HWADDR=00:0c:29:39:af:4c
   IPADDR=192.168.221.129
   NETMASK=255.255.255.0
   GATEWAY=192.168.221.2
   ```

   ```
   HWADDR=00:0c:29:41:4c:0e
   IPADDR=192.168.221.130
   NETMASK=255.255.255.0
   GATEWAY=192.168.221.2
   ```

   ```
   HWADDR=00:0c:29:cf:75:57
   IPADDR=192.168.221.131
   NETMASK=255.255.255.0
   GATEWAY=192.168.221.2
   ```

   多虚拟机网络配置出错

   ![image-20200726144943485](image-20200726144943485.png)

   解决方法：

   ```dave
   [root@www.cndba.cn~]# systemctl stop NetworkManager
   [root@www.cndba.cn~]# systemctl disable NetworkManager
   ```

   

2. 安装MySQL：

   MySQL8在centos7上安装不成功，最后安装的是5.7

3. 配置主库MySQL：

   1. /etc/my.cnf增加配置

      ```
      log_bin=mysql-bin
      server-id=1
      sync-binlog=1
      binlog-ignore-db=performance_schema
      binlog-ignore-db=information_schema
      binlog-ignore-db=sys
      ```

   2. 设置半同步

      ```sql
      -- 引入插件
      INSTALL plugin rpl_semi_sync_master soname 'semisync_master.so';
      -- 查看插件状态
      show VARIABLES like '%semi%';
      -- 设置参数
      set GLOBAL rpl_semi_sync_master_enabled=1;
      set GLOBAL rpl_semi_sync_master_timeout=1000;
      ```

      

   

4. 配置从库MySQL

   - /etc/my.cnf增加配置

     ```
     server-id=2
     relay_log=mysql-relay-bin
     read_only=1
     ```

   - 执行：

     ```sql
     -- 设置主库
     change MASTER to master_host='192.168.221.128',MASTER_port=3306,master_user='root',master_password='root',master_log_file='mysql-bin.000002',master_log_pos=154;
     -- 开启从库
     START SLAVE;
     -- 查看从库状态
     show SLAVE STATUS;
     ```

   - 出现的错误：

     Fatal error: The slave I/O thread stops because master and slave have equal MySQL server UUIDs; these UUIDs must be different for replication to work.

     是由于虚拟机从一台机器上克隆过来的，导致MySQL的uuid重复。

     解决方式：

     ​	1.停止MySQL服务；2.删除/var/lib/mysql/auto.cnf；3.重启

   - 设置半同步：

     ```sql
     INSTALL plugin rpl_semi_sync_slave soname 'semisync_slave.so';
     show VARIABLES like '%semi%';
     set GLOBAL rpl_semi_sync_slave_enabled = 1;
     ```

     

5. 配置MHA

   - 配置host:

     ```
     192.168.221.129    manager
     192.168.221.128    master
     192.168.221.130    slave1
     192.168.221.131    slave2
     
     执行：for i in master slave1 slave2 manager;do scp /etc/hosts $i:/etc/;done
     ```

   - 配置ssh免密登录

     ```
     # 创建密钥对
     ssh-keygen -t rsa
     #将公钥分发到其他节点
     for i in master slave1 slave2 manager;do ssh-copy-id $i;done
     #验证：如果每台主机上执行以下指令不用输入密码就可以获取所有主机的主机名，说明免密登录配置无误
     for i in master slave1 slave2 manager;do ssh $i hostname;done
     ```
     
   - yum install perl-CPAN perl-ExtUtils-CBuilder perl-ExtUtils-MakeMaker
   
   - 更改/etc/masterha/app1.cnfuser=manager           #指定manager管理数据库节点所使用的用户名
   
     ```
     password=manager@123      #对应的是上面用户的密码
     ssh_user=root        #指定配置了ssh免密登录的系统用户
     repl_user=root    #指定用于同步数据的用户名
     repl_password=root@123    #对应的是上面同步用户的 密码
     ping_interval=1        #设置监控主库，发送ping包的时间间隔，默认是3秒，尝试三次没有回应时自动进行切换
     
     [server1]
     hostname=192.168.221.128
     port=3306
     master_binlog_dir=/var/lib/mysql     #指定master保存二进制日志的路径，以便MHA可以找到master的日志
     candidate_master=1     #设置为候选master，设置该参数后，发生主从切换以后将会将此库提升为主库
     
     [server2]
     hostname=192.168.221.130
     port=3306
     master_binlog_dir=/var/lib/mysql
     candidate_master=1      #设置为候选master
     
     [server3]
     hostname=192.168.221.131
     port=3306
     master_binlog_dir=/var/lib/mysql
     no_master=1      #设置的不为备选主库
     ```
   
   - 验证有效性，启动：
   
     ```
     #验证SSH有效性：
     masterha_check_ssh --global_conf=/etc/masterha/masterha_default.cnf --conf=/etc/masterha/app1.cnf
     
     #验证集群复制的有效性（MySQL必须都启动）
     masterha_check_repl --global_conf=/etc/masterha/masterha_default.cnf --conf=/etc/masterha/app1.cnf
     
     #启动
     nohup masterha_manager --conf=/etc/masterha/app1.cnf &> /var/log/mha_manager.log &
     ```


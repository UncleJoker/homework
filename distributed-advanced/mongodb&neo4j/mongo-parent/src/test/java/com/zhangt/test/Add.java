package com.zhangt.test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Add {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("139.199.19.5", 27017);
        MongoDatabase database = mongoClient.getDatabase("zhangt");
        MongoCollection<Document> collection = database.getCollection("abc");

        Document parse = Document.parse("{name:'zhangt2',age:288,hobby:'篮球'}");
        collection.insertOne(parse);
        mongoClient.close();
    }
}

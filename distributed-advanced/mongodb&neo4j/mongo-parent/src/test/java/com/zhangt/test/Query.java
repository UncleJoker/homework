package com.zhangt.test;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

public class Query {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("139.199.19.5", 27017);
        MongoDatabase database = mongoClient.getDatabase("zhangt");
        MongoCollection<Document> collection = database.getCollection("abc");

        FindIterable<Document> documents = collection.find();
        for (Document document : documents) {
            System.out.println(document);
        }
        mongoClient.close();
    }
}

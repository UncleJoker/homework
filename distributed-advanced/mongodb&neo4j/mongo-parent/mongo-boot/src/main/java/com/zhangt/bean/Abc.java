package com.zhangt.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class Abc {

    private String id;

    private String name;

    private String age;


    private String hobby;

    public Abc() {
    }

    public Abc(String id, String name, String age, String hobby) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.hobby = hobby;
    }
}

package com.zhangt.dao;

import com.zhangt.bean.LgResumeDatas;

import java.util.List;

public interface LgResumeDatasDao {


    List<LgResumeDatas> queryByName(String name);
}

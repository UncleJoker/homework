package com.zhangt.dao.impl;

import com.zhangt.bean.LgResumeDatas;
import com.zhangt.dao.LgResumeDatasDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LgResumeDatasDaoImpl implements LgResumeDatasDao {

    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public List<LgResumeDatas> queryByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoTemplate.find(query, LgResumeDatas.class);
    }
}

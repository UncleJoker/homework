package com.zhangt.dao.impl;

import com.zhangt.bean.Abc;
import com.zhangt.dao.AbcDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AbcDaoImpl implements AbcDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void insertAbc(Abc abc) {
        mongoTemplate.insert(abc);
    }

    @Override
    public List<Abc> queryByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoTemplate.find(query, Abc.class);
    }
}

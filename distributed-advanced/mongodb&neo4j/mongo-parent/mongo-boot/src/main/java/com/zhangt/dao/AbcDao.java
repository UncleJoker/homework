package com.zhangt.dao;

import com.zhangt.bean.Abc;

import java.util.List;

public interface AbcDao {

    void insertAbc(Abc abc);

    List<Abc> queryByName(String name);
}

package com.zhangt.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;


@Getter
@Setter
@ToString
@Document(collection = "lg_resume_datas")
public class LgResumeDatas {

    private String id;

    private String name;

    private String salary;

    public LgResumeDatas() {
    }

    public LgResumeDatas(String id, String name, String salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }
}

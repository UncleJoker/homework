package com.zhangt;

import com.zhangt.bean.Abc;
import com.zhangt.bean.LgResumeDatas;
import com.zhangt.dao.AbcDao;
import com.zhangt.dao.LgResumeDatasDao;
import com.zhangt.repository.AbcRepository;
import com.zhangt.repository.LgResumeDatasRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class MongoBootApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(MongoBootApplication.class, args);

        LgResumeDatasRepository abcRepository = applicationContext.getBean(LgResumeDatasRepository.class);
        List<LgResumeDatas> all = abcRepository.findAll();
        System.out.println(all.size());
        for (LgResumeDatas abc : all) {
            System.out.println(abc);
        }

//        LgResumeDatasDao dao = applicationContext.getBean(LgResumeDatasDao.class);
//        List<LgResumeDatas> test1 = dao.queryByName("test1");
//        System.out.println(test1.size());
    }

}

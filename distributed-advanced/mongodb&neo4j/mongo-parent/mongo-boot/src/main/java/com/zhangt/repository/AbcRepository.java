package com.zhangt.repository;

import com.zhangt.bean.Abc;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AbcRepository extends MongoRepository<Abc, String> {
}

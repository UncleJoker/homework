package com.zhangt.repository;

import com.zhangt.bean.LgResumeDatas;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LgResumeDatasRepository extends MongoRepository<LgResumeDatas, String> {
}

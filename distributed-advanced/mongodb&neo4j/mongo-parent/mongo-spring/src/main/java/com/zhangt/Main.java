package com.zhangt;

import com.zhangt.bean.Abc;
import com.zhangt.dao.AbcDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        AbcDao abcDao = applicationContext.getBean(AbcDao.class);

//        abcDao.insertAbc(new Abc(null, "zhangt123", "18","足球"));
        List<Abc> zhangt2 = abcDao.queryByName("zhangt");
        for (Abc abc : zhangt2) {
            System.out.println(abc);
        }
    }
}

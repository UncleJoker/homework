package com.zhangt.reducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class NumSortReducer extends Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {

    public static int sorNum = 1;

    public synchronized static int getSorNum() {
        int result = sorNum;
        sorNum = sorNum + 1;
        return result;
    }

    @Override
    protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

        System.out.println("传入的key：" + key);
        for (IntWritable count : values) {
            System.out.println("reduce收到的val:" + count.get());
            int sorNum = NumSortReducer.getSorNum();
            IntWritable intWritable = new IntWritable(sorNum);
            context.write(intWritable, key);
        }

    }
}

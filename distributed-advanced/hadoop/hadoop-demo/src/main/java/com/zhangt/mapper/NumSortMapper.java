package com.zhangt.mapper;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class NumSortMapper extends Mapper<LongWritable, Text, IntWritable, IntWritable> {

    IntWritable outVal = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
//        super.map(key, value, context);
        // 1 获取一行
        String val = value.toString();
        System.out.println("读取的val:" + val);
        IntWritable intWritable = new IntWritable(Integer.parseInt(val));
        context.write(intWritable, outVal);

    }
}

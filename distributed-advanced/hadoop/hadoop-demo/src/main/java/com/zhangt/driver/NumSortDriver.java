package com.zhangt.driver;

import com.zhangt.mapper.NumSortMapper;
import com.zhangt.reducer.NumSortReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class NumSortDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        // 1 获取配置信息以及封装任务
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration);
        // 2 设置jar加载路径
        job.setJarByClass(NumSortDriver.class);
        // 3 设置map和reduce类
        job.setMapperClass(NumSortMapper.class);
        job.setReducerClass(NumSortReducer.class);
        // 4 设置map输出
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(IntWritable.class);
        // 5 设置最终输出kv类型

        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(IntWritable.class);
        // 6 设置输入和输出路径
        Path[] paths = new Path[3];
        paths[0] = new Path(args[0]);
        paths[1] = new Path(args[1]);
        paths[2] = new Path(args[2]);
        FileInputFormat.setInputPaths(job, paths);
        FileOutputFormat.setOutputPath(job, new Path(args[3]));
        // 7 提交b
        boolean result = job.waitForCompletion(true);
        System.exit(result ? 0 : 1);
    }
}

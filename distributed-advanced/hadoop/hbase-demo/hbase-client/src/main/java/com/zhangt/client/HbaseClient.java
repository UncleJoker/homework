package com.zhangt.client;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

public class HbaseClient {
    static Configuration conf = null;
    static Connection conn = null;

    static {
        conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "master");
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        try {
            conn = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void create() throws IOException {
        final Admin admin = conn.getAdmin();
        HTableDescriptor user = new HTableDescriptor(TableName.valueOf("relation"));
        user.addFamily(new HColumnDescriptor("friends"));
        admin.createTable(user);
    }

    //某个用户其中一个好友，
    public void deleteFriends(String uid, String friend) throws IOException {
        /*
        1 需要根据传入用户删除其列族中对应好友的列
        2 其好友的列族对应需要删除该用户的列
         */
        final Table relation = conn.getTable(TableName.valueOf("relation"));
        final Delete delete = new Delete(Bytes.toBytes(uid));
        delete.addColumn(Bytes.toBytes("friends"), Bytes.toBytes(friend));//最好使用协处理器实现
//        postDelete(delete);
        relation.delete(delete);

    }

    public void postDelete(Delete delete) {
        final byte[] rowkeyUid = delete.getRow();//获取rowkey
        //获取到所有的cell对象
        final NavigableMap<byte[], List<Cell>> familyCellMap = delete.getFamilyCellMap();
        final Set<Map.Entry<byte[], List<Cell>>> entries = familyCellMap.entrySet();
        for (Map.Entry<byte[], List<Cell>> entry : entries) {
            System.out.println(Bytes.toString(entry.getKey()));//列族信息
            final List<Cell> cells = entry.getValue();
            for (Cell cell : cells) {
                final byte[] rowkey = CellUtil.cloneRow(cell);//rowkey信息
                System.out.println(new String(rowkey));
                final byte[] column = CellUtil.cloneQualifier(cell);//列信息
                System.out.println(new String(column));
                //验证删除的目标数据是否存在，存在则执行删除否则不执行,必须有此判断否则造成协处理器被循环调用耗尽资源
            }
        }
    }


    //初始化部分数据
    @Test
    public void initRelationData() throws IOException {
        final Table relation = conn.getTable(TableName.valueOf("relation"));
        //插入uid1用户
        final Put uid1 = new Put(Bytes.toBytes("uid1"));
        uid1.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid2"), Bytes.toBytes("uid2"));
        uid1.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid3"), Bytes.toBytes("uid3"));
        uid1.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid4"), Bytes.toBytes("uid4"));
        //插入uid2用户
        final Put uid2 = new Put(Bytes.toBytes("uid2"));
        uid2.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid1"), Bytes.toBytes("uid1"));
        uid2.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid3"), Bytes.toBytes("uid3"));
        uid2.addColumn(Bytes.toBytes("friends"), Bytes.toBytes("uid4"), Bytes.toBytes("uid4"));
        //准备list集合
        final ArrayList<Put> puts = new ArrayList<>();
        puts.add(uid1);
        puts.add(uid2);
        //执行写入
        relation.put(puts);
        System.out.println("数据初始化成功！！");
    }

    public static void main(String[] args) throws IOException {
        final HbaseClient r = new HbaseClient();
//        r.create();//创建表
//        r.initRelationData();//初始化数据
        //假设uid1用户删除了uid2这个好友，需要执行删除操作
        r.deleteFriends("uid1", "uid2");
    }
}

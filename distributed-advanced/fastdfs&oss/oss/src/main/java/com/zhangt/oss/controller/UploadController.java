package com.zhangt.oss.controller;


import com.aliyun.oss.model.OSSObject;
import com.zhangt.oss.bean.UpLoadResult;
import com.zhangt.oss.service.FileUpLoadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Controller
@RequestMapping("/pic")
public class UploadController {
    @Autowired
    private FileUpLoadService fileUpLoadService;


    @PostMapping("/upload")
    @ResponseBody
    public UpLoadResult upload(@RequestParam("file") MultipartFile multipartFile) {
        return fileUpLoadService.upload(multipartFile);
    }

    @PostMapping("/delete")
    @ResponseBody
    public String delete(String keyName) {
        try {
            fileUpLoadService.delete(keyName);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "failed";
        }
    }

    @GetMapping("/download")
    @ResponseBody
    public ResponseEntity<byte[]> download(String keyName) throws IOException {
        OSSObject ossObject = fileUpLoadService.download(keyName);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 4];
        int n = 0;
        while (-1 != (n = ossObject.getObjectContent().read(buffer))) {
            output.write(buffer, 0, n);
        }
        byte[] result = output.toByteArray();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("charset", "utf-8");
        responseHeaders.setContentType(MediaType.valueOf("text/html"));
        responseHeaders.setContentLength(result.length);
        responseHeaders.set("Content-disposition", "attachment; filename=ok.jpg");


        return new ResponseEntity<>(result, responseHeaders, HttpStatus.OK);

    }
}

### fastdfs部署文档

1. 安装fastdfs

2. fastdfs-nginx-module

3. LuaJIT-2.0.4

4. ngx_devel_kit v0.2.18

5. lua-nginx-module v0.8.10

6. nginx-1.4.2

   ./configure --add-module=/root/fastdfs/ngx_devel_kit-0.3.0  --add-module=/root/fastdfs/lua-nginx-module-0.10.8 --add-module=/root/fastdfs/fastdfs-nginx-module-1.20/src

7. GraphicsMagick

   gm -v验证是否支持jpg/png

   安装jpg支持：yum install -y libpng-devel libpng

   安装png支持：yum install -y  libjpeg-devel libjpeg
   
   nginx配置：
   
   ```
   server {
       	listen	8888;
       	server_name	localhost;
           set $img_thumbnail_root /resource/image;
           set $img_file $img_thumbnail_root$uri;
       	
           location ~* ^(\/(\w+)(\/M00)(.+\.(jpg|jpeg|gif|png))_(\d+)+x(\d+)+\.(jpg|jpeg|gif|png))$ {
               root $img_thumbnail_root;    # root path for croped img
               set $fdfs_group_root /home/fastdfs/data; #set fastdfs group path $2
   
               if (!-f $img_file) {   # if thumb file not exists
                       add_header X-Powered-By 'Nginx+Lua+GraphicsMagick By Yanue';  #  header for test
                       add_header file-path $request_filename;    #  header for test
                       set $request_filepath $fdfs_group_root$4;    # real file path
                       set $img_width $6;    #  img width
                       set $img_height $7;    #  img height
                       set $img_ext $5;     # file ext
                       content_by_lua_file /usr/local/nginx/lua/cropSize.lua;    # load crop Lua file
               }
           }
           location ~/group[0-9]/ {
               ngx_fastdfs_module;
           }
        }
   ```
   
   lua脚本：
   
   ```lua
   -- 根据输入长和宽的尺寸裁切图片
   
   -- 检测路径是否目录
   local function is_dir(sPath)
       if type(sPath) ~= "string" then return false end
   
       local response = os.execute("cd " .. sPath)
       if response == 0 then
           return true
       end
       return false
   end
   
   -- 文件是否存在
   function file_exists(name)
       local f = io.open(name, "r")
       if f ~= nil then io.close(f) return true else return false end
   end
   
   -- 获取文件路径
   function getFileDir(filename)
       return string.match(filename, "(.+)/[^/]*%.%w+$") --*nix system
   end
   
   -- 获取文件名
   function strippath(filename)
       return string.match(filename, ".+/([^/]*%.%w+)$") -- *nix system
   end
   
   --去除扩展名
   function stripextension(filename)
       local idx = filename:match(".+()%.%w+$")
       if (idx) then
           return filename:sub(1, idx - 1)
       else
           return filename
       end
   end
   
   --获取扩展名
   function getExtension(filename)
       return filename:match(".+%.(%w+)$")
   end
   
   -- 开始执行
   -- ngx.log(ngx.ERR, getFileDir(ngx.var.img_file));
   
   local gm_path = '/usr/local/GraphicsMagick/bin/gm'
   
   -- check image dir
   if not is_dir(getFileDir(ngx.var.img_file)) then
       os.execute("mkdir -p " .. getFileDir(ngx.var.img_file))
   end
   
       ngx.log(ngx.ERR,ngx.var.img_file);
       ngx.log(ngx.ERR,ngx.var.request_filepath);
   -- 裁剪后保证等比缩图 （缺点：裁剪了图片的一部分）
   -- gm convert input.jpg -thumbnail "100x100^" -gravity center -extent 100x100 output_3.jpg
   if (file_exists(ngx.var.request_filepath)) then
       local cmd = gm_path .. ' convert ' .. ngx.var.request_filepath
       cmd = cmd .. " -thumbnail " .. ngx.var.img_width .. "x" .. ngx.var.img_height .. "^"
       cmd = cmd .. " -gravity center -extent " .. ngx.var.img_width .. "x" .. ngx.var.img_height
   --  cmd = cmd .. " -quality 100"
       cmd = cmd .. " +profile \"*\" " .. ngx.var.img_file;
   --  ngx.log(ngx.ERR, cmd);
       os.execute(cmd);
       ngx.exec(ngx.var.uri);
   else
       ngx.exit(ngx.HTTP_NOT_FOUND);
   end
   
   ```
   
   
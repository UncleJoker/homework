package com.zhangt.config;


import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitConfig {

    public final static String ORDER_QUEUE_NAME = "order_queue";
    public final static String ORDER_QUEUE_DLX_NAME = "order_queue_dlx";
    /**
     * 死信交换器
     *
     * @return
     */
    @Bean("exchangeDlx")
    public Exchange exchangeDlx() {
        return new DirectExchange("ex_dlx", true, false, null);
    }

    @Bean("queueDlx")
    public Queue queueDlx() {
        return QueueBuilder.durable(ORDER_QUEUE_DLX_NAME).build();
    }

    //绑定  将队列和交换机绑定, 并设置用于匹配键：TestDirectRouting
    @Bean
    public Binding bindingDlx() {
        return BindingBuilder.bind(queueDlx()).to(exchangeDlx()).with("dlx_routing").noargs();
    }

    @Bean("orderQueue")
    public Queue directQueue() {
        // durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：当前连接有效
        // exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
        // autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。
        Map<String, Object> props = new HashMap<>();
        // 消息的生存时间 10s
        props.put("x-message-ttl", 10000);
        // 设置该队列所关联的死信交换器（当队列消息TTL到期后依然没有消费，则加入死信队列）
        props.put("x-dead-letter-exchange", "ex_dlx");
        // 设置该队列所关联的死信交换器的routingKey，如果没有特殊指定，使用原队列的routingKey
        props.put("x-dead-letter-routing-key", "dlx_routing");
        return new Queue(ORDER_QUEUE_NAME, true, false, false, props);

    }

    @Bean("orderExchange")
    public DirectExchange DirectExchange() {
        return new DirectExchange("DirectExchange", true, false);
    }

    //绑定  将队列和交换机绑定, 并设置用于匹配键：TestDirectRouting
    @Bean
    public Binding bindingDirect() {
        return BindingBuilder.bind(directQueue()).to(DirectExchange()).with("DirectRouting");
    }

    public static void main(String[] args) {
        String s = "FMZRqerectoiNSHz65jkuMsR4zxj1vbQ7oa";

        System.out.println(s.length());
    }
}

package com.zhangt.consumer;

import com.zhangt.config.RabbitConfig;
import com.zhangt.entity.Order;
import com.zhangt.repository.OrderRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
public class OrderConsumer {

    @Autowired
    private OrderRepository orderRepository;

    @RabbitListener(queues = RabbitConfig.ORDER_QUEUE_DLX_NAME)
    public void orderListenDlx(String msg) {

        System.out.println("死信队列收到的消息："+msg);
        Integer id  = Integer.valueOf(msg);
        Optional<Order> byId = orderRepository.findById(id);
        if(byId.isPresent()){
            Order order = byId.get();
            if(StringUtils.isEmpty(order.getStatus())){
                order.setStatus("订单超时");
                orderRepository.save(order);
            }
        }else{
            System.out.println("无对应订单");
        }
    }
}

package com.zhangt.controller;

import com.zhangt.entity.Order;
import com.zhangt.repository.OrderRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class PayController {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private OrderRepository orderRepository;

    @GetMapping("/downOrder/{orderId}")
    public String down(@PathVariable Integer orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        orderRepository.save(order);
        rabbitTemplate.convertAndSend("DirectExchange", "DirectRouting", order.getId());
        return "下单成功";
    }

    @GetMapping("/pay/{orderId}")
    public String pay(@PathVariable Integer orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        Optional<Order> one = orderRepository.findOne(Example.of(order));
        if (one.isPresent()) {
            Order orderEx = one.get();
            orderEx.setStatus("已支付");
            orderRepository.save(orderEx);
            return "支付成功";
        }
        return "无对应订单";
    }

}

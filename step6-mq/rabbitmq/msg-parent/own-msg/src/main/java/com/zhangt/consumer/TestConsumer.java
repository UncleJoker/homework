package com.zhangt.consumer;

import com.zhangt.entity.Msg;

import java.util.concurrent.BlockingQueue;

public class TestConsumer  implements Runnable{

    private final BlockingQueue<Msg> blockingQueue;

    public TestConsumer(BlockingQueue<Msg> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        while(true) {
            try{
                Thread.sleep(100);
                long startTime = System.currentTimeMillis();
                Msg msg = blockingQueue.take();
                long endTime = System.currentTimeMillis();
                System.out.println("我消费了口罩："+msg+", 等到货时我阻塞了"+ (endTime-startTime) +"ms");
            }catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}

package com.zhangt.provider;

import com.zhangt.entity.Msg;

import java.util.UUID;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class TestProvider implements Runnable {

    private final BlockingQueue<Msg> blockingQueue;

    public TestProvider(BlockingQueue<Msg> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        while (true){
            try {
                if(blockingQueue.remainingCapacity() > 0) {
                    Msg msg = new Msg(UUID.randomUUID().toString(), "N95");
                    blockingQueue.add(msg);
                    System.out.println("我在生产口罩，当前库存是："+ blockingQueue.size());
                }else {
                    System.out.println("我的仓库已经堆满了"+blockingQueue.size() +"个口罩，快来买口罩啊！");
                }
                Thread.sleep(200);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

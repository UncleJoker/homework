package com.zhangt;

import com.zhangt.consumer.TestConsumer;
import com.zhangt.entity.Msg;
import com.zhangt.provider.TestProvider;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class App {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Msg> blockingQueue = new ArrayBlockingQueue<>(20);
        TestProvider testProvider = new TestProvider(blockingQueue);
        TestConsumer testConsumer = new TestConsumer(blockingQueue);
        new Thread(testProvider).start();
        Thread.sleep(20000);
        new Thread(testConsumer).start();
    }
}

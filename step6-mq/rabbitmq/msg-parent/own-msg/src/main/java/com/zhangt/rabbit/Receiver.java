package com.zhangt.rabbit;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class Receiver {

    public static void main(String[] args) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException {

        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUri("amqp://admin:123456@192.168.221.132:5672");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
//        GetResponse getResponse = channel.basicGet(Sender.QUEUE_NAME, true);
//        byte[] body = getResponse.getBody();
//        System.out.println(new String(body));
        channel.basicConsume(Sender.QUEUE_NAME, (s, delivery) -> {
            System.out.println(new String(delivery.getBody()));
        }, (consumerTag) -> {
        });
    }
}

package com.zhangt.mvcframework.demo.controller;

import com.zhangt.mvcframework.annotations.Autowired;
import com.zhangt.mvcframework.annotations.Controller;
import com.zhangt.mvcframework.annotations.RequestMapping;
import com.zhangt.mvcframework.annotations.Security;
import com.zhangt.mvcframework.demo.service.IDemoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@Security("zt")
@RequestMapping("/class")
public class ClassController {

    @Autowired
    private IDemoService demoService;


    @Security(value = "zt")
    @RequestMapping("/handle01")
    public String handle01(HttpServletRequest req, HttpServletResponse res, String username) {
        System.out.println("handle01");
        return demoService.get(username);
    }

    @Security(value = {"zt", "hy"})
    @RequestMapping("/handle02")
    public String handle02(HttpServletRequest req, HttpServletResponse res, String username) {
        System.out.println("handle02");
        return demoService.get(username);
    }

    @Security(value = "hy")
    @RequestMapping("/handle03")
    public String handle03(HttpServletRequest req, HttpServletResponse res, String username) {
        System.out.println("handle03");
        return demoService.get(username);
    }
}

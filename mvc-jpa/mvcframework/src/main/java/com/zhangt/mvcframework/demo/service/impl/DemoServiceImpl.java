package com.zhangt.mvcframework.demo.service.impl;

import com.zhangt.mvcframework.annotations.Service;
import com.zhangt.mvcframework.demo.service.IDemoService;

@Service
public class DemoServiceImpl implements IDemoService {
    @Override
    public String get(String name) {
        System.out.println("进入service");
        return "有权限的:"+name;
    }
}

package com.zhangt.mvcframework.servlet;

import com.zhangt.mvcframework.annotations.*;
import com.zhangt.mvcframework.pojo.Handler;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DispatchServlet extends HttpServlet {

    private Properties properties = new Properties();

    /*
        1.加载配置文件：doLoadConfig
        2.扫描注解
        3.初始化bean对象
        4.实现依赖注入
        5.构造一个处理器映射器，将配置好的url和method建立映射关系
    */
    @Override
    public void init(ServletConfig config) throws ServletException {
        String contextConfigLocation = config.getInitParameter("contextConfigLocation");
        doLoadConfig(contextConfigLocation);

        doScan(properties.getProperty("scanPackage"));

        doInstance();
        doAutowired();
        initHandlerMapping();
        System.out.println("spring mvc初始化完成");
    }

    private List<Handler> handlerList = new ArrayList<>();

    private void initHandlerMapping() {
        if (iocMap.isEmpty()) {
            return;
        }
        try {
            for (Map.Entry<String, Object> entry : iocMap.entrySet()) {
                Class<?> aClass = entry.getValue().getClass();
                if (!aClass.isAnnotationPresent(Controller.class)) {
                    continue;
                }
                String baseUrl = "";
                if (aClass.isAnnotationPresent(RequestMapping.class)) {
                    RequestMapping annotation = aClass.getAnnotation(RequestMapping.class);
                    String value = annotation.value();
                    if (!value.startsWith("/")) {
                        value = "/" + value;
                    }
                    baseUrl = value;
                }
                for (Method method : aClass.getMethods()) {
                    if (!method.isAnnotationPresent(RequestMapping.class)) {
                        continue;
                    }
                    RequestMapping annotation = method.getAnnotation(RequestMapping.class);
                    String value = annotation.value();
                    if (!value.startsWith("/")) {
                        value = "/" + value;
                    }
                    String url = baseUrl + value;
                    Handler handler = new Handler(entry.getValue(), method, Pattern.compile(url));
                    Parameter[] parameters = method.getParameters();
                    for (int i = 0; i < parameters.length; i++) {
                        Parameter parameter = parameters[i];
                        if (parameter.getType() == HttpServletRequest.class || parameter.getType() == HttpServletResponse.class) {
                            handler.getParamIndexMap().put(parameter.getType().getSimpleName(), i);
                        } else {
                            handler.getParamIndexMap().put(parameter.getName(), i);
                        }
                    }
                    handlerList.add(handler);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doAutowired() {
        if (iocMap.isEmpty()) {
            return;
        }
        try {
            for (Map.Entry<String, Object> entry : iocMap.entrySet()) {
                Field[] declaredFields = entry.getValue().getClass().getDeclaredFields();
                for (Field field : declaredFields) {
                    if (!field.isAnnotationPresent(Autowired.class)) {
                        continue;
                    }
                    Autowired annotation = field.getAnnotation(Autowired.class);
                    String beanName = annotation.value();
                    if ("".equals(beanName.trim())) {
                        beanName = field.getType().getName();
                    }
                    field.setAccessible(true);
                    field.set(entry.getValue(), iocMap.get(beanName));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Map<String, Object> iocMap = new HashMap<>();

    private void doInstance() {
        if (classNames.size() == 0) {
            return;
        }
        try {
            for (int i = 0; i < classNames.size(); i++) {
                String classPath = classNames.get(i);
                Class<?> aClass = Class.forName(classPath);
                if (aClass.isAnnotationPresent(Controller.class)) {
                    String simpleName = aClass.getSimpleName();
                    String keyName = lowerFirst(simpleName);
                    Object o = aClass.newInstance();
                    iocMap.put(keyName, o);
                } else if (aClass.isAnnotationPresent(Service.class)) {
                    Service annotation = aClass.getAnnotation(Service.class);
                    String beanName = annotation.name();
                    if ("".equals(beanName.trim())) {
                        beanName = lowerFirst(aClass.getSimpleName());
                    }
                    Object o = aClass.newInstance();
                    iocMap.put(beanName, o);
                    Class<?>[] interfaces = aClass.getInterfaces();
                    for (int j = 0; j < interfaces.length; j++) {
                        Class<?> aInterface = interfaces[j];
                        iocMap.put(aInterface.getName(), o);
                    }
                } else {
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String lowerFirst(String str) {
        char[] chars = str.toCharArray();
        if ('A' <= chars[0] && chars[0] <= 'Z') {
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }

    private List<String> classNames = new ArrayList<>();

    private void doScan(String packageName) {
        String s = packageName.replaceAll("\\.", "/");
        String pkgPath = Thread.currentThread().getContextClassLoader().getResource("").getPath() + s;
        File pkgParent = new File(pkgPath);
        File[] files = pkgParent.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                doScan(packageName + "." + file.getName());
            } else if (file.getName().endsWith(".class")) {
                String classPath = packageName + "." + file.getName().replaceAll(".class", "");
                classNames.add(classPath);
            }
        }
    }

    private void doLoadConfig(String contextConfigLocation) {

        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation);
        try {
            assert resourceAsStream != null;
            properties.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Handler handler = getHandler(req);
        if (handler == null) {
            resp.getWriter().write("404 not found");
            return;
        }
        Class<?>[] parameterTypes = handler.getMethod().getParameterTypes();
        Object[] params = new Object[parameterTypes.length];
        //获取参数名为username的参数值
        String username = null;
        Map<String, String[]> parameterMap = req.getParameterMap();
        for (Map.Entry<String, String[]> stringEntry : parameterMap.entrySet()) {
            String value = StringUtils.join(stringEntry.getValue(), ",");
            if (handler.getParamIndexMap().get(stringEntry.getKey()) != null) {
                params[handler.getParamIndexMap().get(stringEntry.getKey())] = value;
            }
            if (stringEntry.getKey().equals("username")) {
                username = value;
            }
        }
        if (!"".equals(username)) {
            Security security = null;
            if(handler.getController().getClass().isAnnotationPresent(Security.class)) {
                security = handler.getController().getClass().getAnnotation(Security.class);
            }else {
                if(handler.getMethod().isAnnotationPresent(Security.class)) {
                    security = handler.getMethod().getAnnotation(Security.class);
                }
            }
            if (security != null) {
                String[] authStrS = security.value();
                boolean hasAuth = false;
                if (authStrS.length > 0) {
                    for (String authStr : authStrS) {
                        hasAuth = authStr.equals(username);
                        if (hasAuth) {
                            break;
                        }
                    }
                }
                if (!hasAuth) {
                    resp.getWriter().write("501 no authorization");
                    return;
                }
            }
        }

        int requestIndex = handler.getParamIndexMap().get(HttpServletRequest.class.getSimpleName());
        params[requestIndex] = req;
        int respIndex = handler.getParamIndexMap().get(HttpServletResponse.class.getSimpleName());
        params[respIndex] = resp;
        try {
            resp.getWriter().write(String.valueOf(handler.getMethod().invoke(handler.getController(), params)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler getHandler(HttpServletRequest request) {
        if (handlerList.size() == 0) {
            return null;
        }
        String uri = request.getRequestURI();
        System.out.println("请求uri" + uri);
        for (Handler handler : handlerList) {
            Matcher matcher = handler.getPattern().matcher(uri);
            if (!matcher.matches()) {
                continue;
            }
            return handler;
        }
        return null;
    }
}

package com.zhangt.mvcframework.demo.service;

public interface IDemoService {

    String get(String name);
}

package com.zhangt.mvcframework.pojo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Handler {

    private Object controller;

    private Method method;

    private Pattern pattern;

    private Map<String, Integer> paramIndexMap; //参数顺序，用来绑定key参数名，value代表第一个参数

    public Handler(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMap = new HashMap<>();
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Map<String, Integer> getParamIndexMap() {
        return paramIndexMap;
    }

    public void setParamIndexMap(Map<String, Integer> paramIndexMap) {
        this.paramIndexMap = paramIndexMap;
    }
}

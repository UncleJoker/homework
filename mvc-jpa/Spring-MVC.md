#### Spring-MVC:



![image-20200409140957401](image-20200409140957401.png)

##### spring-mvc的核心类：

​	1.前端控制器：DispatchServlet

​	2.处理器映射：HandlerMapping

​	3.处理器适配器：HandlerAdapter

​	4.试图解析器：ViewResolver

##### spring-mvc请求处理流程：![image-20200409225513077](image-20200409225513077.png)

##### 	九大组件：

​	1.处理器映射：HandlerMapping：处理url请求路径

​	2.处理器适配器：HandlerAdapter：处理参数绑定

​	3.视图解析器：ViewResolver：将String类型的视图名解析成View类型的视图。即找到渲染所用的模板和所				用的技术并填入参数

​	4.RequestToViewNameTranslator：从请求中获取ViewName

​	5.LocaleResolver 国际化，处理区域

​	6.ThemeResolver：主题解析器

​	7.MultipartResolver：上传请求处理，封装普通的请求，使其拥有文件上传的功能

​	8.FlashMapManager：FlashMap用于重定向的参数传递，FlashMapManager用来管理FlashMap

​	9.HandlerExceptionResolver异常的视图处理

##### URL-PATTERN

配置url-pattern的时候 “/”会拦截静态资源，.html、.css、.js等

原因：tomcat容器（父）中web.xml配置的defactServlet 使用的是“/”，当mvc容器（子）的web.xml配置的url-pattern为“/”时，会覆盖tomcat容器中web.xml的配置。而父容器中的.jsp是有单独的servlet进行处理，所以不被覆盖

##### 监听器、过滤器、拦截器

过滤器：Filter，作用在servlet之前，配置为“/*”对所有的资源进行过滤

监听器：Listener，实现javax.servlet.servletContextListener接口，web容器启动时启动，只启动一次，一直运行之web容器的销毁

拦截器：是mvc、struts表现层框架自己的，不会拦截静态资源，只拦截Handler。配置在框架的配置文件中。

![image-20200410125758671](image-20200410125758671.png)


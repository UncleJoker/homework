<%@ page contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>列表</title>
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script>
        $(function () {
            $(".del").bind("click", function () {
                var $tr = $(this).parent();
                var id = $tr.children(':first').html();
                console.log(id)
                // 发送ajax请求
                $.ajax({
                    url: '/operate/delete?id=' + id,
                    type: 'POST',
                    success: function (data) {
                        alert(data);
                        window.location.reload()
                    }
                })
            })

            $(".update").bind("click", function () {
                var $tr = $(this).parent();
                var btn = $(this).children(":first");
                var id = $tr.children(':first').html();
                if (btn.html() == "修改") {
                    $tr.children().children(":input").removeAttr("readonly")
                    $(this).children(":first").html("保存")
                } else {
                    var name = $tr.find("input[class=name]").val()
                    var address = $tr.find("input[class=address]").val()
                    var phone = $tr.find("input[class=phone]").val()
                    $.ajax({
                        url: '/operate/update',
                        type: 'POST',
                        data: '{"id":"'+id+'","name":"'+name+'","address":"'+address+'","phone":"'+phone+'"}',
                        contentType: 'application/json;charset=utf-8',
                        dataType: 'json',
                        success: function (data) {
                            if(data != null) {
                                alert("保存成功");
                                window.location.reload();
                            }else {
                                alert("保存失败");
                            }
                        }
                    })
                }
            })

            $("#member_add").bind("click",function () {
                $('.add-dialog').show();
                $('.add-dialog #confirm').click(function () {
                    var name = $('.add-dialog .name').val();
                    var address = $('.add-dialog .address').val();
                    var phone = $('.add-dialog .phone').val();
                    $.ajax({
                        url: '/operate/add',
                        type: 'POST',
                        data: '{"name":"'+name+'","address":"'+address+'","phone":"'+phone+'"}',
                        contentType: 'application/json;charset=utf-8',
                        dataType: 'json',
                        success: function (data) {
                            if(data != null) {
                                alert("保存成功");
                                window.location.reload();
                            }else {
                                alert("保存失败");
                            }
                        }
                    })
                })
                $(".add-dialog #concel").click(function () {
                    $('.add-dialog').hide();
                    $code = $('.add-dialog .name').val('');
                    $name = $('.add-dialog .address').val('');
                    $sex = $('.add-dialog .phone').val('');
                })
            })
        })
    </script>
</head>
<body>
<div>
    <h1 style="color:red">列表</h1>
    <input type="button" id="member_add" value="新增"/>
    <table border="0">
        <tr>
            <td>地址</td>
            <td>姓名</td>
            <td>电话</td>
        </tr>
        <c:forEach items="${list}" var="item">
            <tr>
                <td style="display: none">${item.id}</td>
                <td><input type="text" class="name" readonly="readonly" value="${item.name}"/></td>
                <td><input type="text" class="address" readonly="readonly" value="${item.address}"/></td>
                <td><input type="text" class="phone" readonly="readonly" value="${item.phone}"/></td>
                <td class="update"><a href="#">修改</a></td>
                <td class="del"><a href="#">删除</a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="add-dialog" style="display: none">
    <div class="header">添加</div>
    <div>姓名<input type="text" class="name"></div>
    <div>地址<input type="text" class="address"></div>
    <div>电话<input type="text" class="phone"></div>
    <button id="confirm">确定</button>
    <button id="concel">取消</button>
</div>
</body>
</html>

package com.zhangt.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class LoginInterceptor implements HandlerInterceptor {

    private static List<String> uriList = new ArrayList<>();

    static {
        uriList.add("/login");
        uriList.add("/login.html");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String requestURI = request.getRequestURI();
        if (uriList.contains(requestURI)) {
            return true;
        }
        String auth = (String) request.getSession().getAttribute("auth");
        if (auth != null && !"".equals(auth) && auth.equals("admin")) {
            return true;
        } else {
            response.sendRedirect("/login.html");
            return false;
        }

    }
}

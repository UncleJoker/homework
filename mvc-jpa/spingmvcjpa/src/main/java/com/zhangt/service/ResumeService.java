package com.zhangt.service;

import com.zhangt.pojo.Resume;

import java.util.List;

public interface ResumeService {

    List<Resume> findAll();

    String deleteById(Long id);

    Resume update(Resume resume);

    Resume add(Resume resume);
}

package com.zhangt.service.impl;

import com.zhangt.dao.ResumeDao;
import com.zhangt.pojo.Resume;
import com.zhangt.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeDao resumeDao;

    @Override
    public List<Resume> findAll() {

        return resumeDao.findAll();
    }

    @Override
    public String deleteById(Long id) {
        Optional<Resume> byId = resumeDao.findById(id);
        if(byId.isEmpty()) {
            return "error";
        }else {
            resumeDao.deleteById(id);
            return "success";
        }
    }

    @Override
    public Resume update(Resume resume) {
        return resumeDao.save(resume);
    }

    @Override
    public Resume add(Resume resume) {
        return resumeDao.save(resume);
    }
}

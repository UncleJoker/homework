package com.zhangt.test;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

public class JedisTest {

    public static void main(String[] args) {
        JedisPoolConfig config=new JedisPoolConfig();
        Set<HostAndPort> jedisClusterNode=new HashSet<HostAndPort>();
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6379));
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6380));
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6381));
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6382));
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6383));
        jedisClusterNode.add(new HostAndPort("192.168.221.128", 6384));
        JedisCluster jcd=new JedisCluster(jedisClusterNode, config);
        jcd.set("name:001","zhangfei");
        String value=jcd.get("name:001");
        System.out.println(value);
    }
}

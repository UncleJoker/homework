#### Redis三主三从安装部署

1.安装库文件

- yum -y install make automake cmake gcc g++
- make MALLOC=libc

2.安装ruby依赖

- yum -y install ruby rubygems

3.安装redis

- wget http://download.redis.io/releases/redis-5.0.5.tar.gz
- make & make install

4.创建cluster配置文件

- mkdir cluster && cd cluster

- cp ../redis.conf redis-master1.conf

  cp ../redis.conf redis-master2.conf

  cp ../redis.conf redis-master3.conf

  cp ../redis.conf redis-slave1.conf

  cp ../redis.conf redis-slave2.conf

  cp ../redis.conf redis-slave3.conf

  修改配置文件端口、cluster-config-file、cluster-enabled、pidfile 

- 编辑start-redis.sh

  ```
  redis-server redis-master1.conf
  redis-server redis-master2.conf
  redis-server redis-master3.conf
  redis-server redis-slave1.conf
  redis-server redis-slave2.conf
  redis-server redis-slave3.conf
  ```

- chmod u+x  start.sh

- redis-cli --cluster create 192.168.221.128:6379 192.168.221.128:6380 192.168.221.128:6381 192.168.221.128:6382 192.168.221.128:6383 192.168.221.128:6384 --cluster-replicas 1

5.扩容

- cp redis.master1.conf redis-6385.conf
- 修改配置文件相关内容
- redis-cli --cluster add-node 192.168.221.128:6385 192.168.221.128:6379
- 查看集群信息：cluster nodes
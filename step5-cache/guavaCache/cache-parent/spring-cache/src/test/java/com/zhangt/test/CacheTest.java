package com.zhangt.test;

import com.zhangt.cache.RedisCacheApplication;
import com.zhangt.cache.entity.Position;
import com.zhangt.cache.entity.User;
import com.zhangt.cache.service.PositionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisCacheApplication.class)
public class CacheTest {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private PositionService positionService;

    @Test
    public void findUserTest() {

        for (int i = 0; i < 3; i++) {
            System.out.println("第" + i + "次");
            User user = this.findUser();
            System.out.println(user);
        }
    }

    @Cacheable(value = {"valueName", "valueName2"}, key = "'keyName1'")
    public User findUser() {
        System.out.println("执行方法...");
        return new User("id1", "张三", "深圳", "1234567", 18);
    }
}

package com.zhangt.cache.service;



import com.zhangt.cache.entity.Position;
import com.zhangt.cache.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    @Cacheable(value = "position:all")
    public List<Position> findAllPosition() {

        return positionRepository.findAll();
    }
}

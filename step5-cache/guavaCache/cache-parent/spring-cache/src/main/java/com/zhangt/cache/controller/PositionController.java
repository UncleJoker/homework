package com.zhangt.cache.controller;

import com.zhangt.cache.entity.Position;
import com.zhangt.cache.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("position")
@RestController
public class PositionController {

    @Autowired
    private PositionService positionService;

    @GetMapping("all")
    public List<Position> findList() {

        return positionService.findAllPosition();
    }
}

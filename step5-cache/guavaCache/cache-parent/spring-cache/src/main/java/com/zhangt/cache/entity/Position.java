package com.zhangt.cache.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "position")
public class Position implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//主键生成策略
    private Long id;

    private String title;

    private Date publishDate;

    private String workYear;

    private String edu;

    private String label;

    private String beginSalary;

    private String endSalary;

    private String company;

    private String companyLabel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getWorkYear() {
        return workYear;
    }

    public void setWorkYear(String workYear) {
        this.workYear = workYear;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBeginSalary() {
        return beginSalary;
    }

    public void setBeginSalary(String beginSalary) {
        this.beginSalary = beginSalary;
    }

    public String getEndSalary() {
        return endSalary;
    }

    public void setEndSalary(String endSalary) {
        this.endSalary = endSalary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyLabel() {
        return companyLabel;
    }

    public void setCompanyLabel(String companyLabel) {
        this.companyLabel = companyLabel;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", publishDate=" + publishDate +
                ", workYear='" + workYear + '\'' +
                ", edu='" + edu + '\'' +
                ", label='" + label + '\'' +
                ", beginSalary='" + beginSalary + '\'' +
                ", endSalary='" + endSalary + '\'' +
                ", company='" + company + '\'' +
                ", companyLabel='" + companyLabel + '\'' +
                '}';
    }
}

package com.zhangt.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.zhangt.entity.Position;
import com.zhangt.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class GlobalCache {

    @Autowired
    private PositionService positionService;

    @Bean("guavaCache")
    public LoadingCache<String, Object> initCache() {
        //CacheLoader的方式创建
        return CacheBuilder.newBuilder()
                /*
                     加附加的功能
                 */
                //弱值的删除
                .maximumSize(3).weakValues()
                .build(new CacheLoader<String, Object>() {
                    @Override
                    public Object load(String o) {
                        return null;
                    }
                });
    }

    /**
     * 读取缓存数据 如果没有则回调源数据并(自动)写入缓存
     *
     * @param key
     * @param cache
     */
    public Object get(String key, LoadingCache<String, Object> cache) throws ExecutionException {
        //回调方法用于读源并写入缓存
        return cache.get(key, () -> {
            Object value = null;
            if (key.equalsIgnoreCase("all")) {
                List<Position> allPosition = positionService.findAllPosition();
                value = allPosition;
                cache.put(key, allPosition);
            }else {
                Position position = positionService.findById(Integer.valueOf(key));
                value = position;
                cache.put(key, position);
            }
            //写回缓存
            return value;
        });
    }

    /**
     * 显示缓存里的数据
     *
     * @param cache
     */
    public void display(LoadingCache<String, Object> cache) {
        //利用迭代器
        Iterator its = cache.asMap().entrySet().iterator();
        while (its.hasNext()) {
            System.out.println(its.next().toString());
        }
    }
}

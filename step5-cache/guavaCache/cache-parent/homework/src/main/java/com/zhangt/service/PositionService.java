package com.zhangt.service;


import com.zhangt.entity.Position;
import com.zhangt.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    @Cacheable("position:all")
    public List<Position> findAllPosition() {

        return positionRepository.findAll();
    }

    @Cacheable(value = "position:id", key = "#id")
    public Position findById(Integer id) {
        Optional<Position> position = positionRepository.findById(Long.valueOf(id));
        return position.orElse(null);
    }
}

package com.zhangt.controller;

import com.zhangt.cache.GlobalCache;
import com.zhangt.entity.Position;
import com.google.common.cache.LoadingCache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RequestMapping("position")
@RestController
public class PositionController {

    @Autowired
    private GlobalCache globalCache;
    @Autowired
    private LoadingCache<String, Object> guavaCache;

    @GetMapping("all")
    public List<Position> findList() throws ExecutionException {

        return (List<Position>) globalCache.get("all", guavaCache);
    }

    @GetMapping("query/{id}")
    public Position findList(@PathVariable Integer id) throws ExecutionException {

        return (Position) globalCache.get(id+"", guavaCache);
    }
    @GetMapping("display")
    public void display(){

        globalCache.display(guavaCache);
    }
}

package config;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import server.HttpServlet;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Configuration {

    private Mapper mapper;

    public Configuration() {
        this.loadServer();
    }

    public Mapper getMapper() {
        return mapper;
    }

    public void loadServer() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");

        SAXReader saxReader = new SAXReader();
        try {
            if (mapper == null) {
                mapper = new Mapper();
            }
            Document read = saxReader.read(resourceAsStream);
            Element rootElement = read.getRootElement();
            List<Element> serverNodes = rootElement.selectNodes("//server");
            for (Element selectNode : serverNodes) {
                List<Mapper.MapperHost> hostList = new ArrayList<>();
                List<Element> serviceNodes = selectNode.selectNodes("//service");
                for (Element serviceNode : serviceNodes) {
                    Element connNode = (Element) serviceNode.selectSingleNode("//connector");
                    //获取connector端口
                    String portStr = connNode.attributeValue("port");
                    mapper.setPort(Integer.valueOf(portStr));

                    Node engineNode = serviceNode.selectSingleNode("//engine");
                    List<Element> hostNodes = engineNode.selectNodes("//Host");
                    for (Element hostNode : hostNodes) {
                        Mapper.MapperHost mapperHost = new Mapper.MapperHost();
                        String host = hostNode.attributeValue("name");
                        mapperHost.setHost(host);
                        String path = hostNode.attributeValue("appBase");
                        List<Mapper.MapperContext> contextList = loadContext(path);
                        mapperHost.setContextList(contextList);
                        hostList.add(mapperHost);
                    }
                }
                mapper.setHostList(hostList);
            }

            System.out.println("封装结束");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Mapper.MapperContext> loadContext(String path) throws Exception {
        // webapps
        File appFile = new File(path);
        List<Mapper.MapperContext> contextList = new ArrayList<>();

        if (appFile.exists() && appFile.isDirectory()) {
            File[] files = appFile.listFiles();
            for (File projectFile : files) {
                if (projectFile.isDirectory()) {

                    Mapper.MapperContext mapperContext = new Mapper.MapperContext();
                    String projectName = projectFile.getName();
                    mapperContext.setContextName(projectName);
                    List<Mapper.MapperWrapper> wrapperList = new ArrayList<>();
                    mapperContext.setWrapperList(wrapperList);

                    for (File file : projectFile.listFiles()) {
                        String fileName = file.getName();
                        String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
                        if (file.getName().equals("web.xml")) {
                            loadServlet(wrapperList, file);
                        } else if (prefix.equals("html") || prefix.equals("js") || prefix.equals("jpg")) {
                            Mapper.MapperWrapper mapperWrapper = new Mapper.MapperWrapper();
                            mapperWrapper.setResource(true);
                            mapperWrapper.setPath(file.getAbsolutePath());
                            mapperWrapper.setUri(file.getName());
                            wrapperList.add(mapperWrapper);
                        } else {
                            //不做任何操作
                        }
                    }
                    contextList.add(mapperContext);
                }
            }
        }
        return contextList;
    }

    private void loadServlet(List<Mapper.MapperWrapper> wrapperList, File file) throws Exception {
        SAXReader saxReader = new SAXReader();
        Map<String, HttpServlet> servletMap = new HashMap<>();
        Document read = saxReader.read(file);
        Element rootElement = read.getRootElement();
        List selectNodes = rootElement.selectNodes("//servlet");
        for (int i = 0; i < selectNodes.size(); i++) {
            Element element = (Element) selectNodes.get(i);
            Element servletNameEl = (Element) element.selectSingleNode("servlet-name");
            String servletName = servletNameEl.getStringValue();
            Element servletClassEl = (Element) element.selectSingleNode("servlet-class");

            Element servletMappingEl = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
            Element urlPatternEl = (Element) servletMappingEl.selectSingleNode("url-pattern");
            String urlPattern = urlPatternEl.getStringValue();

            Mapper.MapperWrapper mapperWrapper = new Mapper.MapperWrapper();
            mapperWrapper.setUri(urlPattern);
            mapperWrapper.setResource(false);

            String servletPath = servletClassEl.getStringValue().replace(".", "\\");
            String classPath = file.getParentFile().getAbsolutePath() + "\\" + servletPath + ".class";
            mapperWrapper.setPath(classPath);
            wrapperList.add(mapperWrapper);
        }
    }

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.loadServer();

    }
}

package config;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class MiniClassLoad extends ClassLoader  {

    private String classLoadName;

    public MiniClassLoad(String classLoadName) {
        this.classLoadName = classLoadName;
    }

    @Override
    protected Class<?> findClass(String classPath) throws ClassNotFoundException {

        File file = new File(classPath);
        String fileName = classPath.substring(classPath.lastIndexOf("\\") + 1);
        String[] nameArr = fileName.split("\\.");
        String name = nameArr[0];
        try {
            FileInputStream is = new FileInputStream(file);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int len = 0;
            try {
                while ((len = is.read()) != -1) {
                    bos.write(len);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            byte[] data = bos.toByteArray();
            is.close();
            bos.close();

            return defineClass(name, data, 0, data.length);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return super.findClass(name);
    }


    @Override
    public URL getResource(String name) {
        return super.getResource(name);
    }

}

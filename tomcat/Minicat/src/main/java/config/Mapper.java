package config;

import java.util.List;

public class Mapper {

    private List<MapperHost> hostList;
    private Integer port;
    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
    public List<MapperHost> getHostList() {
        return hostList;
    }

    public void setHostList(List<MapperHost> hostList) {
        this.hostList = hostList;
    }

    public static class MapperHost {



        private List<MapperContext> contextList;
        private String host;
        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }


        public List<MapperContext> getContextList() {
            return contextList;
        }

        public void setContextList(List<MapperContext> contextList) {
            this.contextList = contextList;
        }
    }

    public static class MapperContext {

        private String contextName;

        private List<MapperWrapper> wrapperList;

        public String getContextName() {
            return contextName;
        }

        public void setContextName(String contextName) {
            this.contextName = contextName;
        }

        public List<MapperWrapper> getWrapperList() {
            return wrapperList;
        }

        public void setWrapperList(List<MapperWrapper> wrapperList) {
            this.wrapperList = wrapperList;
        }
    }

    public static class MapperWrapper {

        private String uri;

        private String path;

        private boolean isResource;

        public boolean isResource() {
            return isResource;
        }

        public void setResource(boolean resource) {
            isResource = resource;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

    }
}

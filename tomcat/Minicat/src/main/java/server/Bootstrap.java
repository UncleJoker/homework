package server;

import config.Configuration;
import config.Mapper;
import config.MiniClassLoad;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class Bootstrap {


    /**
     * 启动与初始化
     */
    public void start() throws Exception {

        int corePoolSize = 10;
        int maximumPoolSize = 50;
        long keepAliveTime = 100L;
        TimeUnit unit = TimeUnit.MICROSECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(50);
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);


        //1.0
//        while (true) {
//            Socket socket = serverSocket.accept();
//            OutputStream outputStream = socket.getOutputStream();
//            String str = "Hello Minicat";
//            String data = HttpProtocolUtil.getHttpHeader200(str.getBytes().length)+str;
//            outputStream.write(data.getBytes());
//        }
        //2.0
//        while (true) {
//            Socket socket = serverSocket.accept();
//            InputStream inputStream = socket.getInputStream();
//            Request request = new Request(inputStream);
//
//            Response response = new Response(socket.getOutputStream());
//
//            response.outputHtml(request.getUrl());
//            socket.close();
//        }
        Configuration configuration = new Configuration();
        Mapper mapper = configuration.getMapper();
        Map<String, Map<String, Map<String, Object>>> hostMap = initServlet(mapper);
        ServerSocket serverSocket = new ServerSocket(mapper.getPort());
        System.out.println("########端口监听启动：" + mapper.getPort());
        //3.0
        while (true) {
            Socket socket = serverSocket.accept();
            RequestProcessor requestProcessor = new RequestProcessor(socket, hostMap);
            threadPoolExecutor.execute(requestProcessor);
        }


    }

    private Map<String, Map<String, Map<String, Object>>> initServlet(Mapper mapper) throws Exception {
        Map<String, Map<String, Map<String, Object>>> hostMap = new HashMap<>();
        for (Mapper.MapperHost mapperHost : mapper.getHostList()) {
            String host = mapperHost.getHost();
            List<Mapper.MapperContext> contextList = mapperHost.getContextList();
            Map<String, Map<String, Object>> contextMap = new HashMap<>();
            for (Mapper.MapperContext mapperContext : contextList) {
                String contextName = mapperContext.getContextName();
                List<Mapper.MapperWrapper> wrapperList = mapperContext.getWrapperList();
                Map<String, Object> wrapperMap = new HashMap<>();
                for (Mapper.MapperWrapper mapperWrapper : wrapperList) {
                    String uri = mapperWrapper.getUri();
                    if(mapperWrapper.isResource()) {
                        wrapperMap.put(uri, mapperWrapper.getPath());
                    }else {
                        String classPath = mapperWrapper.getPath();
                        Class clazz = new MiniClassLoad("mini").loadClass(classPath);
                        wrapperMap.put(uri, clazz.getDeclaredConstructor().newInstance());
                    }
                }
                contextMap.put(contextName, wrapperMap);
            }
            hostMap.put(host, contextMap);
        }
        return  hostMap;
    }


    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();

//        Class clazz = new MiniClassLoad("mini").loadClass("E:\\webapps\\demo1\\server\\MyServlet.class");
//        clazz.getDeclaredConstructor().newInstance();
    }
}

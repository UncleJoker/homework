package server;

import config.Mapper;

import java.io.InputStream;
import java.net.Socket;
import java.util.Map;

public class RequestProcessor implements Runnable {

    private Socket socket;

    private Map<String, Map<String, Map<String, Object>>> hostMap;

    public RequestProcessor(Socket socket, Map<String, Map<String, Map<String, Object>>> hostMap) {
        this.socket = socket;
        this.hostMap = hostMap;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = socket.getInputStream();
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());
            Map<String, Object> servletMap = hostMap.get(request.getHost()).get(request.getProject());
            if(servletMap != null) {
                if (servletMap.get(request.getUrl()) == null) {
                    response.outputHtml((String) servletMap.get("index.html"));
                } else {
                    HttpServlet httpServlet = (HttpServlet)servletMap.get(request.getUrl());
                    httpServlet.service(request, response);
                }
            }

            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

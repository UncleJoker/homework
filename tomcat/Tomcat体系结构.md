#### Tomcat体系结构

##### 1.概述：

Tomcat的顶层容器是Server，而且一个Tomcat对应一个Server，一个server有多个service提供服务。service包含两个重要组件：Connector和Container。 Server由Catalina来管理，它是tomcat的管理类，它的三个方法load，start，stop分别用来管理整个服务器的生命周期，通过await方法来保证主线程不退出。

##### 2.架构总体设计：

![image-20200422234854294](C:\Users\49914\AppData\Roaming\Typora\typora-user-images\image-20200422234854294.png)

​	**一个Tomcat对应一个Server，一个server有多个service提供服务，一个service对包含多个Connector和一个container。**

##### 3.启用过程：

**Catalina的启动过程**
		Catalina的启动主要调用setAwait，load和start方法来完成。setAwait方法用于设置Server启动后是否进入等待状态的标志，为true进入，否则不进入。load方法用于加载配置文件，start方法用于启动服务器。
		**Server的启动过程**
　　Server接口中提供了addService(Service service)，removeService(Service service)来增加和删除Service，Server中的init和start方法循环调用Service中的init和start方法来启动所有Service。Server的默认实现是StandardServer。
		**Service的启动过程**
　　Service的默认实现是StandardService，和StandardServer一样也继承自LifecycleMBeanBase类，所以init和start方法最终会调用initInternal和startInternal方法。而StandardService中的initInternal和startInternal方法主要调用container，executors，mapperListener，connectors的init和start方法。

**Tomcat通过Lifecycle接口统一管理生命周期,所有生命周期组件都要实现这个接口**


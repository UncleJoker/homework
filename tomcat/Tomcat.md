### Tomcat

#### 1.目录结构

1.bin：脚本目录

2.conf：配置文件存放

​	tomcat-user.xml tomcat用户管理

​	server.xml：配置组件

3.web.xml：全局配置，自己项目的局部配置覆盖全局配置

4.lib：jar包

5.work：jsp编译的过程文件

#### 2.核心功能

**1.http服务器**：核心组件：**Connector**，负责和客户端交互（socket通信）

​	Coyote:

​		EndPoint：通信端点，通信监听的接口。实现tcp/ip协议

​		Processor：应用协议解析处理接口，实现http协议，接收Endpoint的socket，解析成Tomcat Request			和Response对象。通过Adapter提交到容器

​		ProtocolHandler：针对具体协议的处理能力

​		Adapter：适配器

**2.servlet容器**：核心组件：**Container**，处理业务逻辑（servlet容器）

​	Catalina：**（tomcat的核心）**

​		一个Catalina实例对应一个Server实例对应多个Service实例，每个service可以有多个Connector实例和			一个Container实例。**一个Catalina实例就相当于是一个tomcat**

![image-20200422234854294](C:\Users\49914\AppData\Roaming\Typora\typora-user-images\image-20200422234854294.png)

#### 3.迷你tomcat实现关键：

1.socket监听端口

2.outputstream输出内容

3.输出内容要包含标准的请求头格式

4.输出静态文件同样用OutputStream，文件内容大可以分批读取，分批输出

#### 4.源码

问题：

​	1.Catalina为什么要用反射来写，不直接new？这里效果应该是一样的？

​	2.lifecycle的init()方法是如何保证调用的实现方法是lifecycleBase中的init()方法？答：实现类是由Server接口创建的，Server接口只有一个实现类

​	3.模板方法是如何确定交由具体哪一个子类来调用的？

​	4.tomcat类加载机制具体是如何实现不走双亲委派机制的？

**初始化**：tomcat启动初始化依托于Lifecycle接口定义的init()和start()方法。

#### Tomcat优化：

1.JVM虚拟机优化（优化内存模型）

​	-Xms：最小堆内存（与-Xmx设置相同）

​	-Xmx：最大堆内存（设置为可用内存的80%）

​	GC：

​		![image-20200425150130620](C:\Users\49914\AppData\Roaming\Typora\typora-user-images\image-20200425150130620.png)

2.tomcat自身配置优化（是否使用共享线程池？IO模型）

#### nginx

nginx多进程为什么可以监听相同的端口？如果是master进程监听的端口，那是如何分配给work进程的？
package com.zhangt.ioc.pojo;

import java.lang.reflect.Field;

/**
 * @author 应癫
 */
public class Result {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Result{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public static void main(String[] args) throws IllegalAccessException {
        Result result = new Result();
//        result.setStatus("234");
//        result.setMessage("345");
        Field[] fields = result.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
//            field
            field.set(result, "123");
        }
        System.out.println(result);
    }
}

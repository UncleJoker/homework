package com.zhangt.ioc.service.impl;


import com.zhangt.ioc.anno.Autowired;
import com.zhangt.ioc.anno.Service;
import com.zhangt.ioc.anno.Transactional;
import com.zhangt.ioc.dao.AccountDao;
import com.zhangt.ioc.pojo.Account;
import com.zhangt.ioc.service.TransferService;

/**
 * @author 应癫
 */
@Service
@Transactional
public class TransferServiceImpl implements TransferService {


    // 最佳状态
    @Autowired
    private AccountDao accountDao;

    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

            Account from = accountDao.queryAccountByCardNo(fromCardNo);
            Account to = accountDao.queryAccountByCardNo(toCardNo);

            from.setMoney(from.getMoney()-money);
            to.setMoney(to.getMoney()+money);

            accountDao.updateAccountByCardNo(to);
//            int c = 1/0;
            accountDao.updateAccountByCardNo(from);

    }
}

package com.zhangt.ioc.proxy;

import com.zhangt.ioc.anno.Autowired;
import com.zhangt.ioc.anno.Service;
import com.zhangt.ioc.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Proxy;

@Service
public class ProxyFactory {

    @Autowired
    private TransactionManager transactionManager;

    public Object getJdkProxy(Object obj) {

        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), (proxy, method, args) -> {

            transactionManager.beginTransaction();
            try {
                Object result = method.invoke(obj, args);
                transactionManager.commit();
                return result;
            } catch (Exception e) {
                transactionManager.rollback();
                throw new Exception();
            }
        });
    }

    public Object getCglibProxy(Object obj) {
        return Enhancer.create(obj.getClass(), (MethodInterceptor) (o, method, objects, methodProxy) -> {
            transactionManager.beginTransaction();
            try {
                Object result = method.invoke(obj, objects);
                transactionManager.commit();
                return result;
            } catch (Exception e) {
                transactionManager.rollback();
                throw new Exception();
            }
        });
    }
}

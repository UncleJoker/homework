package com.zhangt.ioc.proxy;

import com.zhangt.ioc.service.IMath;
import com.zhangt.ioc.service.impl.Plus;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

public class CglibProxy {

    public static void main(String[] args) {

        IMath plus = new Plus();
        IMath proxyPlus = (IMath)Enhancer.create(plus.getClass(), (MethodInterceptor) (o, method, objects, methodProxy) -> {

            for (int i = 0; i < objects.length; i++) {
                objects[i] = (Object) ((int) objects[i] + 1);
            }
            Object result;
            System.out.println(methodProxy.getSuperName());
            result = method.invoke(plus, objects);
            return result;
        });
        System.out.println(proxyPlus.add(2,1));;
        System.out.println(proxyPlus.divide(3,1));;
    }
}

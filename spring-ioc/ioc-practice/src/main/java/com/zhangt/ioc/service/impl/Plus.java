package com.zhangt.ioc.service.impl;


import com.zhangt.ioc.service.IMath;

public class Plus implements IMath {
    @Override
    public Integer add(int a, int b) {
        return a+b;
    }

    @Override
    public String divide(int a, int b) {
        return (a/b)+"";
    }
}

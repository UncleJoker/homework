package com.zhangt.ioc.utils;

public class StringUtil {
    public static String firstLowCase(String s) {

        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return Character.toLowerCase(s.charAt(0)) + s.substring(1);
    }
}

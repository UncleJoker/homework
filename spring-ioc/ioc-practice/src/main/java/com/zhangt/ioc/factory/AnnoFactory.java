package com.zhangt.ioc.factory;

import com.zhangt.ioc.anno.Autowired;
import com.zhangt.ioc.anno.Service;
import com.zhangt.ioc.anno.Transactional;
import com.zhangt.ioc.proxy.ProxyFactory;
import com.zhangt.ioc.utils.StringUtil;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class AnnoFactory {

    public static Map<String, Object> classMap = new HashMap<>();
    public static Map<String, Object> secondMap = new HashMap<>();

    static {
        try {
            String packageName = "com.zhangt.ioc";

            Reflections reflections = new Reflections(packageName);
            Set<Class<?>> autoSets = reflections.getTypesAnnotatedWith(Service.class);
            for (Class<?> autoSet : autoSets) {
                String key = autoSet.getSimpleName();
                if (autoSet.getInterfaces().length > 0) {
                    key = autoSet.getInterfaces()[0].getSimpleName();
                }
                if (!autoSet.getAnnotation(Service.class).value().equals("")) {
                    key = autoSet.getAnnotation(Service.class).value();
                }
                key = StringUtil.firstLowCase(key);
                Object obj = autoSet.newInstance();
                if (fieldInit(obj)) {
                    classMap.put(key, obj);
                } else {
                    secondMap.put(key, obj);
                }
            }
            while (secondMap.size() > 0) {
                Iterator<Map.Entry<String, Object>> it = secondMap.entrySet().iterator();
                while (it.hasNext()) {
                    Entry<String, Object> entry = it.next();
                    if (fieldInit(entry.getValue())) {
                        classMap.put(entry.getKey(), entry.getValue());
                        it.remove();
                    }
                }
            }
            for (Entry<String, Object> map : classMap.entrySet()) {
                Object obj = classMap.get(map.getKey());
                Class<?> clazz = obj.getClass();
                boolean isPoxy = false;
                if (clazz.getAnnotation(Transactional.class) != null) {
                    isPoxy = true;
                }
                for (Method method : clazz.getMethods()) {
                    if (method.getAnnotation(Transactional.class) != null) {
                        isPoxy = true;
                    }
                }
                if (isPoxy) {
                    ProxyFactory proxyFactory = (ProxyFactory) classMap.get("proxyFactory");
                    if(clazz.getInterfaces().length>0) {
                        map.setValue(proxyFactory.getJdkProxy(obj)) ;
                    } else {
                        map.setValue(proxyFactory.getCglibProxy(obj));
                    }
                }else {
                    map.setValue(obj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static boolean fieldInit(Object obj) throws IllegalAccessException {
        boolean finish = true;
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Autowired.class)) {
                if (classMap.get(field.getName()) != null) {
                    field.set(obj, classMap.get(field.getName()));
                } else {
                    finish = false;
                }
            }
        }
        return finish;
    }


    public static void main(String[] args) {
        System.out.println("");
    }
}

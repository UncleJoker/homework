package com.zhangt.ioc.service;

public interface IMath {

    Integer add(int a, int b);

    String divide(int a, int b);
}

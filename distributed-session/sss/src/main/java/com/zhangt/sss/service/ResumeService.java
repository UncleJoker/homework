package com.zhangt.sss.service;


import com.zhangt.sss.pojo.Resume;

import java.util.List;

public interface ResumeService {

    List<Resume> findAll();

    String deleteById(Long id);

    Resume update(Resume resume);

    Resume add(Resume resume);
}

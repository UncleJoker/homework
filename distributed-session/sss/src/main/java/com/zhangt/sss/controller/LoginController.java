package com.zhangt.sss.controller;


import com.zhangt.sss.pojo.Resume;
import com.zhangt.sss.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class LoginController {

    @Autowired
    private ResumeService resumeService;

    @RequestMapping("/")
    public void indexRed(HttpServletResponse response) throws IOException {
        response.sendRedirect("/login");
    }

    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView index = new ModelAndView("index");
        List<Resume> all = resumeService.findAll();
        index.addObject("list", all);
        return index;
    }

    @RequestMapping("/login.html")
    public ModelAndView login() {
        return new ModelAndView("login");
    }

    @ResponseBody
        @RequestMapping("/login")
    public void login(HttpServletRequest request, HttpServletResponse response, String username, String password) throws IOException {

        if (username != null && password != null && username.equals("admin") && password.equals("admin")) {
            HttpSession session = request.getSession();
            session.setAttribute("auth", username);
            response.sendRedirect("/index");
        } else {
            response.sendRedirect("/login.html");
        }
    }
}

package com.zhangt.sss.controller;


import com.zhangt.sss.pojo.Resume;
import com.zhangt.sss.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("operate")
@RestController
public class OperateController {

    @Autowired
    private ResumeService resumeService;

    @RequestMapping("delete")
    public String delete(Long id) {
        return resumeService.deleteById(id);
    }

    @RequestMapping("add")
    public Resume add(@RequestBody Resume resume) {
        return resumeService.add(resume);
    }

    @RequestMapping("update")
    public Resume update(@RequestBody Resume resume) {
        System.out.println(resume.toString());
        return resumeService.update(resume);
    }
}

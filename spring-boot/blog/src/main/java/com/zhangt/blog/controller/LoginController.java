package com.zhangt.blog.controller;

import com.zhangt.blog.pojo.Article;
import com.zhangt.blog.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Calendar;

@Controller
public class LoginController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/toLoginPage")
    public String toLoginPage(Model model) {
        model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        return "login";
    }

    @RequestMapping("/index")
    public String index(Model model, @RequestParam(name = "page", defaultValue = "0") Integer page,
                        @RequestParam(name = "pageSize", defaultValue = "3") Integer pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        Page<Article> articlePage = articleService.findPage(pageable);
        int pageNum = articlePage.getTotalPages()-1;
        int prePageNum = Math.max(articlePage.getNumber() - 1, 0);
        int nextPageNum = Math.min(articlePage.getNumber() + 1, pageNum);
        model.addAttribute("pageNum", pageNum);
        model.addAttribute("prePageNum", prePageNum);
        model.addAttribute("nextPageNum", nextPageNum);
        model.addAttribute("articlePage", articlePage);
        return "client/index";
    }
}

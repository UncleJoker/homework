package com.zhangt.blog.service;


import com.zhangt.blog.pojo.Article;
import com.zhangt.blog.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

//    @Cacheable(cacheNames = "Article.page",unless = "#result==null")
    public Page<Article> findPage(Pageable page) {
        return articleRepository.findAll(page);
    }


}

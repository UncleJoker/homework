package com.zhangt.blog.repository;

import com.zhangt.blog.pojo.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article,Integer> {



}

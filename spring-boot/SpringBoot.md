### SpringBoot

#### 1.概念：快速使用Spring的一种方式

1.约定优于配置

2.spring的缺点：配置是重量级的，并且管理项目的依赖也是很繁琐的

3.起步依赖：解决项目依赖的版本问题。自动配置：springboot自动配置bean

#### 2.依赖管理

1.继承父工程spring-boot-starter-parent，父工程中填写了配置文件的 加载顺序以及明明规则。父工程继承了spring-boot-dependencies，这其中规定了大部分常用框架的版本号，所以当引入的依赖包实在父工程的管理范围内，就不需要填写版本号。

**2.启动核心**：通过@SpringBootApplication设置启动类，该注解的核心是@EnableAutoConfiguration，又通过@AutoConfigurationPackage注册bean

**3.stater**：理解为可插拔的插件。

#### 3.数据访问

1.默认使用spring-data，封装各种XXXTemplate来提供使用

#### 4.缓存管理

**1.使用注解缓存的时候，要注意返回类型如果为类对象，那么该类需要有无参构造方法，不然无法通过缓存数据实例化返回对象。**

#### 5.编写springboot-starter

1.引入依赖

<dependency>   

​	 <groupId>org.springframework.boot</groupId>    

​		<artifactId>spring-boot-autoconfigure</artifactId>    																               <version>2.2.6.RELEASE</version>

</dependency>

2.在resources目录下新建/META-INF/spring.factories，并写入org.springframework.boot.autoconfigure.EnableAutoConfiguration的自动注册对象

3.实现一个Configuration的配置类，将自己的项目配置覆盖默认配置